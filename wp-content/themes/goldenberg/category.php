<?php
/**
 * The category template file
 *
 */

get_header(); ?>
<div class="container">
  <div class="top-content">

    <h3>News & Insights</h3>

  </div>
  <h1 class="page-title"><?php
       printf( __( 'Category: %s', 'goldenberg' ), '<span>' . single_cat_title( '', false ) . '</span>' );
   ?></h1>

<div class="posts-wrap-cat">

<!-- Start the Loop. -->
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

     <?php if ( in_category( '' ) ) : ?>


     <?php else : ?>
       <div class="post wow fadeIn" data-wow-duration="1s" data-wow-delay=".2">
     <?php endif; ?>

       <div class="person">
         <?php echo get_avatar( get_the_author_meta( 'ID' ) ) ?>
       </div>
       <div class="image">
         <?php the_post_thumbnail(); ?>
       </div>
       <div class="row">
         <div class="post-meta">
           <div class="post-date"><?php the_time('F j, Y'); ?></div>
           <a class="postH" href="<?php echo get_permalink(); ?>">
           <div class="post-title"><?php the_title(); ?></div></a>
           <div class="post-categories"><?php the_category(' | '); ?></div>
         </div>
       </div>

     </div> <!-- closes the first div box -->


     <!-- Stop The Loop (but note the "else:" - see next line). -->

    <?php endwhile;

    // Previous/next page navigation.
    the_posts_pagination( array(
      'prev_text'          => __( 'Previous', 'basetheme' ),
      'next_text'          => __( 'Next', 'basetheme' ),
      'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'basetheme' ) . ' </span>',
    ) );


    else : ?>


 <!-- The very first "if" tested to see if there were any Posts to -->
 <!-- display.  This "else" part tells what do if there weren't any. -->
 <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>




 <!-- REALLY stop The Loop. -->
<?php endif; ?>
</div>
</div>
<?php get_footer(); ?>
