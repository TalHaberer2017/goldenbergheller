<?php
/*
	Help refresh cache for stylesheet, main.js, plugins.js.
	This number is appended in basetheme_enqueue()
*/
function cache_bust() { return '050317'; }

/*
	Include theme files.
	Functions and hooks should go in these files, not in functions.php.
*/

/* Setup menus, sidebars, scripts, etc. */
include(get_template_directory() . '/lib/theme.setup.php');

/* Bootstrap menu walker */
include(get_template_directory() . '/lib/theme.bootstrapmenu.php');

/* Register theme Custom Post Types (note: Many CPTs would actually be created by plugins.) */
include(get_template_directory() . '/lib/theme.customposttypes.php');

/* Misc hooks and functions used on many WordPress sites */
include(get_template_directory() . '/lib/theme.helpers.php');

/* Misc hooks and functions more specific to this site */
include(get_template_directory() . '/lib/theme.functions.php');

/* Register theme shortcodes */
include(get_template_directory() . '/lib/theme.shortcodes.php');

/* Siteorigin hooks */
include(get_template_directory() . '/lib/siteorigin-widgets/siteorigin.hooks.php');

class Walker_Nav_Menu_Dropdown extends Walker_Nav_Menu {

    function start_lvl($output, $depth) {    }

    function end_lvl($output, $depth) {    }

    function start_el($output, $item, $depth, $args) {
        // Here is where we create each option.
        $item_output = '';

        // add spacing to the title based on the depth
        $item->title = str_repeat("&amp;nbsp;", $depth * 4) . $item->title;

        // Get the attributes.. Though we likely don't need them for this...
        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' value="'   . esc_attr( $item->url        ) .'"' : '';

        // Add the html
        $item_output .= '<option'. $attributes .'>';
        $item_output .= apply_filters( 'the_title_attribute', $item->title );

        // Add this new item to the output string.
        $output .= $item_output;

    }

    function end_el($output, $item, $depth) {
        // Close the item.
        $output .= "</option>\n";

    }

}

add_action('wp_footer', 'dropdown_menu_scripts');
function dropdown_menu_scripts() {
    ?>
        <script>
          jQuery(document).ready(function ($) {
            $("#drop-nav").change( function() {
                    document.location.href =  $(this).val();
            });
          });
        </script>
    <?php
}
