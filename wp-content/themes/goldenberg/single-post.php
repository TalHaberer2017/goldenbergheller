<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

	<main id="main" class="site-main index-main" role="main">

	<?php if ( have_posts() ) : the_post(); ?>
		<div class="container">
			<div class="row">
				<div class="col col-12 col-lg-10">
					<div class="post-image wow fadeIn" data-wow-duration="1s">
						<?php the_post_thumbnail( $size, $attr ); ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col col-12 col-lg-8">

					<h1 class="the-title wow fadeIn" data-wow-duration="1s" data-wow-delay=".4s"><?php the_title(); ?></h1>
					<img class="line wow fadeIn" data-wow-duration="1s" data-wow-delay=".4s" src="<?php echo get_stylesheet_directory_uri(); ?>/svg/orange-line.svg" alt="Goldenberg & Heller Image">
					<div class="blog-content-wrap wow fadeIn" data-wow-duration="1s" data-wow-delay=".15s">

						<p class="icon-date"><?php echo get_the_date( get_option('date_format') ); ?></p>

						<?php the_content(); ?>

					</div>

				<hr>

				<div class="row align-items-center row-author">
					<div class="col col-3 col-md-2">
						<div class="author">
							<?php echo get_avatar( get_the_author_meta( 'ID' ) ) ?>
						</div>
					</div>
					<div class="col col-6 col-md-6">
					<!-- <a href="<?php echo get_permalink();  ?>"><p class="authorName"><?php the_author(); ?></p></a> -->
						<p class="authorName"> <?php the_author(); ?> </p>
					</div>

				</div>
				</div>

				<div class="col col-12 col-lg-4">
					<div class="sidebar-wrap wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">
							<?php get_sidebar(); ?>
              <ul class="social-icons-row">
                <li><a href="<?php the_field('facebook_link', 'option'); ?>" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/facebook2.png" alt="Facebook Logo"></a></li>
                <li><a href="<?php the_field('linkedin_link', 'option'); ?>" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/linkedin2.png" alt="LinkedIn Logo"></a></li>
                <li><a href="<?php the_field('twitter_link', 'option'); ?>" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/twitter2.png" alt="Twitter Logo"></a></li>

              </ul>
					</div>
					<div class="newsletter-wrap">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/svg/signupLogo.svg" alt="Goldenberg & Heller Logo Signup">
						<h1><?php the_field('newsletter_heading_one', 'option'); ?></h1>
						<p><?php the_field('newsletter_heading_two', 'option'); ?></p>
						<div class="orangeLine"></div>
						<?php echo do_shortcode('[gravityform id="2" title="false" description="false" ajax="true"]'); ?>
					</div>
				</div>

			</div>
		<?php if ( is_home() && ! is_front_page() ) : ?>
			<header>
				<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
			</header>
		<?php endif; ?>

		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			/*
			 * Include the Post-Format-specific template for the content.
			 * If you want to override this in a child theme, then include a file
			 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
			 */
			// get_template_part( 'template-parts/content', get_post_format() );

		// End the loop.
		endwhile;

		// Previous/next page navigation.
		the_posts_pagination( array(
			'prev_text'          => __( 'Previous page', 'basetheme' ),
			'next_text'          => __( 'Next page', 'basetheme' ),
			'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'basetheme' ) . ' </span>',
		) );

	// If no content, include the "No posts found" template.
	else :
		get_template_part( 'template-parts/content', 'none' );

	endif;
	?>

	<?php get_sidebar(); ?>
		</div>
	</main><!-- .site-main -->

<?php get_footer(); ?>
