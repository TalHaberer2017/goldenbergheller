<?php

/* Exclude featured posts from main blog loop */
function exclude_featured_from_blog_index( $query ) {
	if (!is_admin() && $query->is_home() && $query->is_main_query() ) {
		$post_ids = bt_featured_post_ids();
		if (!empty($post_ids)) {
			$query->set( 'post__not_in', $post_ids );
		}
	}
}
add_action( 'pre_get_posts', 'exclude_featured_from_blog_index' );

function cptui_register_my_cpts() {

	/**
	 * Post Type: attorneys.
	 */

	$labels = array(
		"name" => __( "attorneys", "basetheme" ),
		"singular_name" => __( "attorney", "basetheme" ),
		'menu_name'   => __( 'Attorneys', 'text_domain' ),
	);

	$args = array(
		"label" => __( "attorneys", "basetheme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "attorneys", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "http://localhost:8888/wp-content/uploads/2018/04/court.png",
		"supports" => array( "title", "editor", "thumbnail", "excerpt", "custom-fields" ),
		"taxonomies" => array( "category", "post_tag" ),
	);

	register_post_type( "attorneys", $args );

	/**
	 * Post Type: Verdicts.
	 */

	$labels = array(
		"name" => __( "Verdicts", "basetheme" ),
		"singular_name" => __( "verdict", "basetheme" ),
	);

	$args = array(
		"label" => __( "Verdicts", "basetheme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "settlements_verdicts", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "http://localhost:8888/wp-content/uploads/2018/04/trial.png",
		"supports" => array( "title", "editor", "thumbnail" ),
		"taxonomies" => array( "category", "post_tag" ),
	);

	register_post_type( "settlements_verdicts", $args );

	/**
	 * Post Type: Testimonials.
	 */

	$labels = array(
		"name" => __( "Testimonials", "basetheme" ),
		"singular_name" => __( "testimonial", "basetheme" ),
	);

	$args = array(
		"label" => __( "Testimonials", "basetheme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "testimonials", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "http://localhost:8888/wp-content/uploads/2018/04/testimonial.png",
		"supports" => array( "title", "editor", "thumbnail", "excerpt", "custom-fields" ),
		"taxonomies" => array( "category", "post_tag" ),
	);

	register_post_type( "testimonials", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );

/**
 * Post Type: Articles.
 **/

 // Register Custom Post Type
 function custom_post_type() {

 	$labels = array(
 		'name'                  => _x( 'articles', 'Post Type General Name', 'text_domain' ),
 		'singular_name'         => _x( 'article', 'Post Type Singular Name', 'text_domain' ),
 		'menu_name'             => __( 'News', 'text_domain' ),
 		'name_admin_bar'        => __( 'news', 'text_domain' ),
 		'archives'              => __( 'Item Archives', 'text_domain' ),
 		'attributes'            => __( 'Item Attributes', 'text_domain' ),
 		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
 		'all_items'             => __( 'All Items', 'text_domain' ),
 		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
 		'add_new'               => __( 'Add New', 'text_domain' ),
 		'new_item'              => __( 'New Item', 'text_domain' ),
 		'edit_item'             => __( 'Edit Item', 'text_domain' ),
 		'update_item'           => __( 'Update Item', 'text_domain' ),
 		'view_item'             => __( 'View Item', 'text_domain' ),
 		'view_items'            => __( 'View Items', 'text_domain' ),
 		'search_items'          => __( 'Search Item', 'text_domain' ),
 		'not_found'             => __( 'Not found', 'text_domain' ),
 		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
 		'featured_image'        => __( 'Featured Image', 'text_domain' ),
 		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
 		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
 		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
 		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
 		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
 		'items_list'            => __( 'Items list', 'text_domain' ),
 		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
 		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
 	);
 	$args = array(
 		'label'                 => __( 'article', 'text_domain' ),
 		'description'           => __( 'used for news articles', 'text_domain' ),
 		'labels'                => $labels,
 		'supports'              => array( 'title', 'editor' ),
 		'taxonomies'            => array( 'post_tag' ),
 		'hierarchical'          => true,
 		'public'                => true,
 		'show_ui'               => true,
 		'show_in_menu'          => true,
		"menu_icon" => "http://localhost:8888/wp-content/uploads/2018/05/folded-newspaper-1.png",
 		'show_in_admin_bar'     => true,
 		'show_in_nav_menus'     => true,
 		'can_export'            => true,
 		'has_archive'           => true,
 		'exclude_from_search'   => false,
 		'publicly_queryable'    => true,
 		'capability_type'       => 'page',
 	);
 	register_post_type( 'articles', $args );

 }
 add_action( 'init', 'custom_post_type', 0 );
