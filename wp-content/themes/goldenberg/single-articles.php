<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

	<main id="main" class="site-main index-main" role="main">

	<?php if ( have_posts() ) : the_post(); ?>
		<div class="container">
			<div class="row">
				<div class="col col-12 col-md-8">
					<div class="top-content wow fadeIn" data-wow-duration=".5s">
						<p class="icon-date"><?php echo get_the_date( get_option('date_format') ); ?></p>
		          <?php the_content(); ?>
							<div class="spacer"></div>
					</div>
				</div>
				<div class="col col-12 col-md-4">
					<div class="blog-content-wrap wow fadeIn" data-wow-duration="2s" data-wow-delay=".5s">

						<div class="other-news-wrap">
							<h2>Other News</h2>

							<?php
								$articles  = array(
									'post_type' => 'articles',
									'post_status' => 'publish',
									'showposts' => 4
								);
								$loop = new WP_Query($articles  );

								if ($loop->have_posts()) : ?>
								<?php while($loop->have_posts()) : $loop->the_post(); ?>
									<div class="single-news">
												<p class="icon-date"><?php echo get_the_date( get_option('date_format') ); ?></p>
												<div>
												<div class="news-title">
													<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
												</div>
												</div>

								</div>

								<?php endwhile; ?>
								<?php endif; ?>
							<?php wp_reset_postdata(); ?>

							<div class="view-all">
								<a href="<?php echo home_url( '/latest-news' ); ?>">View All News
									<div class="orangeLine"></div>
								</a>
							</div>


						</div>



					</div>
				</div>
				</div>

			</div>
		</div>
				<?php endif; ?>
	</main>

<?php get_footer(); ?>
