<?php get_header(); ?>
<main id="main" class="site-main" role="main">
	<div class="container">
		<div class="404Wrap mx-auto tex-center">
				<?php get_template_part( 'template-parts/content', 'none' ); ?>
				<p>Start typing, then press enter to search.</p>
		</div>

	</div>
</main>
<?php get_footer(); ?>
