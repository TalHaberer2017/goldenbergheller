// Simple State Manager
// Trigger javascript functions at various window sizes.
// Booleans used to limit functions to only run once.
// Adjust window sizes to match bootstrap sizes


const _ssm_settings = {
	tablet_min: 768,
	desktop_min: 992
};

let _bt_mobile_run = false;
let _bt_tablet_run = false;
let _bt_tablet_or_bigger_run = false;
let _bt_desktop_run = false;


(function(){
	ssm.addState({
		id: 'mobile',
		query: '(max-width: ' + (parseInt(_ssm_settings.tablet_min, 10) - 1) + 'px)',
		onEnter: function()
		{
			runMobile();
		}
	});

	ssm.addState({
		id: 'tablet',
		query: '(min-width: ' + parseInt(_ssm_settings.tablet_min, 10) + 'px) and (max-width: ' + (parseInt(_ssm_settings.desktop_min, 10) - 1) + 'px)',
		onEnter: function()
		{
			runTablet();
			runTabletOrBigger();
		}
	});

	ssm.addState({
		id: 'desktop',
		query: '(min-width: ' + parseInt(_ssm_settings.desktop_min, 10) + 'px)',
		onEnter: function()
		{
			runDesktop();
			runTabletOrBigger();
		}
	});
}());

// Mobile
function runMobile() {
	if (!_bt_mobile_run) {
		_bt_mobile_run = true;

			jQuery(document).ready(function($) {



			 });

	}
}

// Tablet
function runTablet() {
	if (!_bt_tablet_run) {
		_bt_tablet_run = true;

	}
}

// Tablet or Desktop
function runTabletOrBigger() {
	if (!_bt_tablet_or_bigger_run) {
		_bt_tablet_or_bigger_run = true;

	}
}

// Desktop
function runDesktop() {
	if (!_bt_desktop_run) {
		_bt_desktop_run = true;

		jQuery(document).ready(function($) {

			$(".dropdown").hover(
			 function() {
					 $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).fadeIn("400");
					 $(this).toggleClass('open');
			 },
			 function() {
					 $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).fadeOut("400");
					 $(this).toggleClass('open');
					 }
			 );
		});
	}
}

jQuery(document).ready(function($) {

			$('.single-attorney:nth-of-type(9) a').click(function(e) {
				e.preventDefault();
		 });


		$('.hamburger').on('click', function() {
			$(this).toggleClass('opened');
		});


      // Slider Functions

	    $('.quotes-slide').slick( {
	      dots: true,
	      infinite: true,
	      speed: 1000,
	      slidesToShow: 1,
	      arrows: false,
	      autoplay: true,
				pauseOnDotsHover: true,
	    });

			$('.verdict-slider').slick({
		    dots: true,
		    arrows: false,
		    infinite: true,
		    speed: 1000,
		    autoplay: true,
		    slidesToShow: 1,
		    centerMode: true,
		    variableWidth: true,
		  });

			$('.slider-home').slick( {
			 dots: true,
			 infinite: true,
			 autoplay: true,
			 speed: 1200,
			 slidesToShow: 1,
			 arrows: false,
			});

      $('.test-slide').slick( {
       dots: true,
       infinite: true,
       speed: 1000,
       slidesToShow: 1,
       arrows: false,
       autoplay: true,
      });

			$('.slide-wrap').slick( {
       dots: true,
       infinite: true,
       speed: 1400,
       slidesToShow: 1,
       fade: true,
       arrows: false,
       autoplay: true,
      });
});
