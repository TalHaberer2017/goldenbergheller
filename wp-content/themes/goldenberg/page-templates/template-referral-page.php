<?php

/* Template Name: Referral Page */

get_header(); ?>

<main id="main" class="site-main index-main" role="main">
<div class="top-wrap">
  <div class="top-section-referral top-section">
      <div class="container">

      <div class="top-content">
        <div class="top-content wow fadeInDown" data-wow-duration="1s">
            <?php the_field ('top_content'); ?>
        </div>
      </div>

    </div>
  </div>
</div>
<div class="middle-section">
  <div class="image-wrap">

    <img class="image1 rellax wow fadeIn img-fluid" data-rellax-percentage="0.5" data-rellax-speed="0.5"  data-wow-duration=".5s" data-wow-delay=".6s"  src="<?php echo get_stylesheet_directory_uri(); ?>/images/graycircle-small.png" alt="Goldenberg & Heller Circle Image">

    <img class="image3 wow fadeIn img-fluid"   data-wow-duration=".5s" data-wow-delay=".9s" src="<?php echo get_stylesheet_directory_uri(); ?>/images/orangeOval.png" alt="Goldenberg & Heller Circle Image">


    <img class="image4 wow fadeIn img-fluid" data-wow-duration=".5s" data-wow-delay=".3s" src="<?php the_field ('circle_image'); ?>" alt="Goldenberg & Heller Circle Image">

    <img class="image5 wow fadeIn img-fluid"   data-wow-duration=".5s" data-wow-delay="1.2s" src="<?php echo get_stylesheet_directory_uri(); ?>/images/blueOval.png" alt="Goldenberg & Heller Circle Image">

  </div>

    <div class="blue-box-wrap wow fadeInUp" data-wow-duration="1s" data-wow-delay=".2s">

      <?php the_field ('box_text'); ?>

    </div>

</div>

<div class="third-section">

  <div class="container">

    <div class="body-content wow fadeIn" data-wow-duration="1.2s">
      <?php the_field ('body_content'); ?>
      <a href="<?php echo home_url( '/our-attorneys' ); ?>">Meet Our Attorneys
        <div class="orangeLine"></div>
      </a>
    </div>

  </div>

  <div class="container-fluid">
    <div class="backImage mx-auto">
      <img class="img-fluid wow fadeIn" data-wow-duration="1.2s" data-wow-delay=".5s" src="<?php the_field ('body_image'); ?>" alt="Goldenberg & Heller Image">
    </div>

  </div>

</div>
</main>






<?php get_footer(); ?>
