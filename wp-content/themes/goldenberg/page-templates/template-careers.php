<?php

/* Template Name: template careers */

get_header(); ?>
<main id="main" class="site-main index-main" role="main">
<div class="page-wrapper">

    <div class="top-section-about top-section">
        <div class="container">

        <div class="top-content">
          <div class="top-content wow fadeInDown" data-wow-duration="1s">
              <?php the_field ('top_content'); ?>
          </div>
        </div>

      </div>
    </div>

    <div class="application-section">
      <div class="container">
        <div class="gravityform-wrapper">

        <div class="row">
          <div class="col-12 col-md-8 column-form">
            <div class="wow fadeIn" data-wow-duration="1s">
                <?php echo do_shortcode('[gravityform id="3" title="true" description="false" ajax="true"]'); ?>
            </div>
          </div>
          <div class="col-12 col-md-4 column-image">
            <div class="image-wrap">

              <img class="image1 rellax wow fadeIn img-fluid" data-rellax-percentage="0.5" data-rellax-speed="0.5"  data-wow-duration=".5s" data-wow-delay=".3s"  src="<?php echo get_stylesheet_directory_uri(); ?>/images/graycircle-small.png" alt="Gray Circle">

              <img class="image2 wow rellax fadeIn img-fluid"
              data-rellax-percentage="0.5" data-rellax-speed="-0.5"  data-wow-duration=".5s" src="<?php echo get_stylesheet_directory_uri(); ?>/images/about-circle2.png" alt="Laywer Circle Image">

              <img class="image3 wow fadeIn img-fluid"   data-wow-duration=".5s" data-wow-delay=".6s" src="<?php echo get_stylesheet_directory_uri(); ?>/images/orangeOval.png" alt="Lawyer Circle Image">


              <img class="image4 wow fadeIn img-fluid" data-wow-duration=".5s" data-wow-delay=".1s" src="<?php the_field ('circle_image'); ?>" alt="Lawyer Circle Image">

              <img class="image5 wow fadeIn img-fluid"   data-wow-duration=".5s" data-wow-delay=".8s" src="<?php echo get_stylesheet_directory_uri(); ?>/images/blueOval.png" alt="Lawyer Circle Image">


            </div>
          </div>

        </div>
      </div>
      </div>

    </div>
    <div class="current-jobs-listing">
      <div class="container">
        <div class="row">
          <h1><?php the_field ('header_one'); ?></h1>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="job-posting-wrap">

              <?php if(get_field('jobs')): ?>

              	<ul>

              	<?php while(has_sub_field('jobs')): ?>
                  <div class="single-job-posting wow fadeIn" data-wow-duration="1s" data-wow-delay=".4s">

                    	<li>
                        <div class="row">
                          <div class="col-12">
                            <?php the_sub_field('job_description'); ?>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-12 col-md-6">
                            <?php the_sub_field('responsibilities'); ?>
                          </div>
                          <div class="col-12 col-md-6">
                            <?php the_sub_field('qualifications'); ?>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-12 ending-message">
                            <?php the_sub_field('ending_message'); ?>
                          </div>
                        </div>


                      </li>

                  </div>
              	<?php endwhile; ?>

              	</ul>

              <?php endif; ?>

            </div>

          </div>

        </div>
      </div>

    </div>
</div>
</main>






<?php get_footer(); ?>
