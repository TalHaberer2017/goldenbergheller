<?php
/* Template Name: homepage */

get_header(); ?>
<main id="main" class="site-main index-main" role="main">
<div class="page-wrapper">

  <?php
		$image = get_field('hero_image');
		if( !empty($image) ): ?>
		<div class="hero-homepage" id="hero-homepage" style="background-image: url(<?php echo $image; ?>); background-position: center top;">
      <div class="container container-hero">
        <div class="row align-items-center">
          <div class="col col-12 col-sm-12 col-lg-6">
            <div class="hero-content wow fadeIn" data-wow-duration="1s" data-wow-delay=".05s" id="hero-content">
              <?php the_field ('hero_content'); ?>
            </div>
          </div>
      </div>
      </div>
		</div>
	<?php endif; ?>

  <!-- whitebox callouts -->
<div class="callout-section">

  <div class="callout-main-box wow fadeInUp" data-wow-delay="800">
    <div class="container">
      <h1 class="text-center"><?php the_field ('callout_main_box_header'); ?></h1>
      <div class="row text-center">
        <div class="col col-12 col-md-4">
          <a class="boxLink" href="<?php the_field ('link_one_url'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/svg/Hmpg_arrow.svg" alt="Homepage Arrow">&nbsp; <?php the_field ('whitebox_link_one'); ?></a>
        </div>
        <div class="col col-12 col-md-4">
          <a class="boxLink" href="<?php the_field ('link_two_url'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/svg/Hmpg_arrow.svg" alt="Homepage Arrow">&nbsp; <?php the_field ('whitebox_link_two'); ?></a>
        </div>
        <div class="col col-12 col-md-4">
          <a class="boxLink" href="<?php the_field ('link_three_url'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/svg/Hmpg_arrow.svg" alt="Homepage Arrow">&nbsp; <?php the_field ('whitebox_link_three'); ?></a>
        </div>

      </div>
      <div class="row">
        <a class="text-center mx-auto difProb" href="<?php the_field ('callout_link_url'); ?>"><?php the_field ('callout_link_text'); ?></a>
      </div>
    </div>
  </div>
  </div>


    <div class="sectionTwo">

      <!-- Practice Areas Links -->

      <div class="practice-areas wow fadeIn" data-wow-duration=".6s">
        <div class="container text-center mx-auto">
          <?php the_field ('practice_areas'); ?>
        </div>
      </div>

      <div class="buttonWrap">
          <a class="text-center mx-auto d-block goldberg-button" href="<?php echo home_url( '/our-attorneys/' ); ?>">Meet our Attorneys</a>
      </div>

    </div>

    <!-- Section Two Ends -->

    <div class="quilt-box wow fadeIn" data-wow-delay="400">
      <div class="container container-quilt">
        <div class="row no-gutters">
          <div class="col col-12 col-lg-6 column1">

            <!-- Slider One Begins -->

            <?php if( have_rows('blue_box_slider') ): ?>

              	<div class="slides slider-home">

              	<?php while( have_rows('blue_box_slider') ): the_row();

              		// vars
              		$contentBlue = get_sub_field('blue_box_content');


              		?>

              		<div class="slider-content">

              			<?php if( $link ): ?>
              				<a href="<?php echo $link; ?>">
              			<?php endif; ?>

                      <div class="row justify-content-center">

                        <div class="text-wrap mx-auto align-middle slide-content">
                            <?php echo $contentBlue; ?>
                        </div>

                        </div>

              			<?php if( $link ): ?>
              				</a>
              			<?php endif; ?>


              		</div>

              	<?php endwhile; ?>

              </div>

              <?php endif; ?>

              <!-- Slider One Ends -->

          </div>
          <div class="col col-12 col-lg-6 column2">

            <div class="slide-wrap">

              <!-- Posts Slider Begins -->

            <?php
              $post = array(
                'post_type' => 'post',
                'post_status' => 'publish',
                'order' => 'DSC',
                'showposts' => 3
              );
              $loop = new WP_Query($post);

              if ($loop->have_posts()) : ?>
              <?php while($loop->have_posts()) : $loop->the_post(); ?>

                <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );?>

                <div class="single-slide" style="background-image: url('<?php echo $backgroundImg[0]; ?>');">

                    <div class="post-meta">
                      <div class="post-date"><?php the_time('F j, Y'); ?></div>
                      <a class="postH" href="<?php echo get_permalink(); ?>">
                      <div class="post-title"><?php the_title(); ?></div></a>

                      <div class="row align-items-center row-author">
                        <div class="col col-3 col-md-2 col-lg-2">
                          <div class="person">
                            <?php echo get_avatar( get_the_author_meta( 'ID' ) ) ?>
                          </div>
                        </div>
                        <div class="col col-6 col-md-6 col-lg-6">
                          <p class="authorName"> <?php the_author(); ?> </p>
                        </div>
                      </div>
                    </div>
                      <img class="gradient" src="<?php echo get_stylesheet_directory_uri(); ?>/images/gradientFeatured2.png" alt="Gradient Image">
                </div>

              <?php endwhile; shuffle($args); ?>

              <?php endif; ?>
            <?php wp_reset_postdata(); ?>

            <!-- Post Slider Ends -->
          </div>

          </div>

        </div>


        <div class="row no-gutters">
          <div class="col col-12 col-lg-6 column3">

            <?php
              $verdicts = array(
                'post_type' => 'settlements_verdicts',
                'post_status' => 'publish',
                'showposts' => 1
              );
              $loop = new WP_Query($verdicts);

              if ($loop->have_posts()) : ?>
              <?php while($loop->have_posts()) : $loop->the_post(); ?>
                <div class="single-verdict-slide">
                  <div class="container">
                    <div class="row align-items-center">
                      <div class="col-12 col-md-5">
                        <div class="verdict-date">
                          <h1><?php the_field ('verdict_date'); ?></h1>
                        </div>
                      </div>
                      <div class="col-12 col-md-7">
                            <h2><?php echo get_field('quilt_box_three_header', 'options'); ?></h2>
                        <div class="verdict-content">
                          <?php the_field ('verdict_content'); ?>
                        </div>
                        <a href="<?php echo home_url( '/notable-settlements-and-verdicts' ); ?>"><?php the_field('button_view', 'option'); ?>
                          <div class="orangeLine"></div>
                        </a>
                      </div>

                    </div>

                  </div>
                </div>


              <?php endwhile; shuffle($args); ?>
              <?php endif; ?>
            <?php wp_reset_postdata(); ?>

          </div>

          <div class="col col-12 col-lg-6 column4">

            <h2><?php the_field ('quilt_four_header'); ?></h2>

            <div class="news-wrap">

            <ul class="articles-wrap">

              <?php
                $articles = array(
                  'post_type' => 'articles',
                  'post_status' => 'publish',
                  'orderby' => 'post_date',
                  'showposts' => 3
                );
                $loop = new WP_Query($articles);

                if ($loop->have_posts()) : ?>
                <?php while($loop->have_posts()) : $loop->the_post(); ?>

                <li class="single-link">
                  <a class="news-link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                </li>

                <?php endwhile; shuffle($args); ?>
                <?php endif; ?>
              <?php wp_reset_postdata(); ?>


            </ul>

          </div>
          <div class="row">
            <a class="button-view" href="<?php echo home_url( '/latest-news' ); ?>"><?php the_field('button_view', 'option'); ?>
              <div class="orangeLine"></div>
            </a>

          </div>
        </div>

      </div>
    </div>
  </div>

 <!-- End of Quilt -->

    <section class="sectionThree">
      <div class="container">
        <div class="row">

          <div class="col col-12 col-md-6">
            <div class="image-chunk-wrapper">

              <img class="graycircle rellax" data-rellax-speed="2" data-rellax-percentage="0.5" src="<?php echo get_stylesheet_directory_uri(); ?>/images/graycircle.png" alt="Lawyer Image Goldenberg & Heller">

              <img class="graycircle-small rellax" data-rellax-percentage="0.5" data-rellax-speed="-1" src="<?php echo get_stylesheet_directory_uri(); ?>/images/graycircle-small.png" alt="Lawyer Image Goldenberg & Heller">

              <img class="orangeOval wow fadeIn" data-wow-duration=".6s" data-wow-delay=".9s" src="<?php echo get_stylesheet_directory_uri(); ?>/images/orangeOval.png" alt="Lawyer Image Goldenberg & Heller">


              <img class="personOne wow fadeIn" data-wow-delay="500" src="<?php the_field ('attorney_image_one'); ?>" alt="Lawyer Image Goldenberg & Heller">

              <img class="personTwo wow fadeInUp" data-wow-delay="600" src="<?php the_field ('attorney_image_two'); ?>" alt="Lawyer Image Goldenberg & Heller">

              <img class="personThree wow fadeInUp" data-wow-delay="700" src="<?php the_field ('attorney_image_three'); ?>" alt="Lawyer Image Goldenberg & Heller">


            </div>
          </div>

          <div class="col col-12 col-md-6">

            <div class="third-section-content wow fadeIn" data-wow-duration="1s">
                  <?php the_field ('third_section_content'); ?>
                  <a href="<?php echo home_url( '/about-goldenberg-heller-antognoli/' ); ?>">Learn about us.
                    <div class="orangeLine"></div>
                  </a>
            </div>

          </div>

        </div>
      </div>

      <div class="container">


        <div class="row">

          <div class="testimonial-slider text-center">
            <div class="slides-quotes">

            <?php
              $verdicts = array(
                'post_type' => 'testimonials',
                'post_status' => 'publish',
                'orderby' => 'date',
                'showposts' => 3
              );
              $loop = new WP_Query($verdicts);

              if ($loop->have_posts()) : ?>
              <div class="quotes-slide">
              <?php while($loop->have_posts()) : $loop->the_post(); ?>
                <div class="container">
                  <div class="row align-items-center">
                    <div class="col col-12">
                      <div class="quote">
                        <h2><?php the_field ('quote'); ?></h2>
                      </div>
                    </div>
                    <div class="col col-12">
                      <div class="quote-author">
                        <?php the_field ('quote_author'); ?>
                      </div>
                    </div>
                  </div>
                </div>


              <?php endwhile; ?>
              </div>

              <?php endif; ?>
            <?php wp_reset_postdata(); ?>
            </div>
          </div>
        </div>

        <div class="row">


          <a class="goldberg-button text-center mx-auto d-block" href="<?php echo home_url( '/contact-goldenberg-heller-antognoli/' ); ?>"><?php the_field('button_consultation', 'option'); ?></a>


        </div>

      </div>
      <div class="container-fluid">
          <div class="associatesListing wow fadeIn" data-wow-delay="500">

            <?php if( have_rows('associates') ): ?>

          	<ul>

          	<?php while( have_rows('associates') ): the_row();

          		// vars
          		$image = get_sub_field('associate_logo');
          		$link = get_sub_field('associate_link');

          		?>

          		<li>

          			<?php if( $link ): ?>
          				<a class="associate-button" href="<?php echo $link; ?>">
          			<?php endif; ?>

          				<img src="<?php echo $image; ?>" alt="<?php echo $image['alt'] ?>" />

          			<?php if( $link ): ?>
          				</a>
          			<?php endif; ?>

          		    <!-- <?php echo $content; ?> -->

          		</li>

          	<?php endwhile; ?>

          	</ul>

          <?php endif; ?>

          </div>


      </div>





    </section>






</div>
</main>

<?php get_footer(); ?>
