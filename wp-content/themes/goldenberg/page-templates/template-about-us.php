<?php

/* Template Name: about us */

get_header(); ?>

<main id="main" class="site-main index-main" role="main">
<div class="page-wrapper">

        <div class="top-section-about top-section">

        <div class="container">

        <div class="top-content">
          <div class="top-content wow fadeInDown" data-wow-duration="1s">
              <?php the_field ('top_content'); ?>
          </div>
        </div>

      </div>
    </div>

    <div class="container">

      <div class="links-wrap wow fadeInUp" data-wow-duration="1s">
          <div class="row">
            <div class="col-6">
              <a class="link1" href="<?php echo home_url( '/our-attorneys/' ); ?>"><?php the_field ('link_text_one'); ?><br>
              <div class="orangeLine"></div>
              </a>
            </div>
            <div class="col-6">
              <a class="link2" href="<?php echo home_url( '/practice-areas' ); ?>"><?php the_field ('link_text_two'); ?><br>
              <div class="orangeLine"></div>
              </a>
            </div>
          </div>
        </div>

    </div>

      <div class="second-section">
        <div class="container">
          <div class="hero-image wow fadeIn" data-wow-duration="2s" data-wow-delay=".2s">
              <img class="about-hero" src="<?php the_field ('about_hero_image'); ?>" alt="Lawyers Image For About Page">
          </div>
          <div class="row text-center max-auto">
            <h2 class="promise-header"><?php the_field ('promise_header'); ?></h2>
          </div>

          <div class="quilt-box">
            <div class="container-fluid">
              <div class="row">
                <div class="col col-12 col-sm-6 quilt quilt1 wow fadeIn" data-wow-duration-"1s">
                  <?php the_field ('quilt_content_one'); ?>
                </div>
                <div class="col col-12 col-sm-6 quilt quilt2 wow fadeIn" data-wow-duration-"1s" data-wow-delay=".1s">
                  <?php the_field ('quilt_content_two'); ?>
                </div>

              </div>

              <div class="row">
                <div class="col col-12 col-sm-6 quilt quilt3 wow fadeIn" data-wow-duration-"1s" data-wow-delay=".2s">
                  <?php the_field ('quilt_content_three'); ?>

                </div>
                <div class="col col-12 col-sm-6 quilt quilt4 wow fadeIn" data-wow-duration-"1s" data-wow-delay=".3s">
                  <?php the_field ('quilt_content_four'); ?>
                </div>

              </div>
            </div>
          </div>


          <div class="button-wrapper">
              <a class="goldberg-button middle-button text-center" href="<?php echo home_url( '/contact-goldenberg-heller-antognoli/' ); ?>"><?php the_field('button_consultation', 'option'); ?></a>
          </div>

        </div>
      </div>

      <div class="third-section-about">
        <div class="container">
          <div class="image-wrap">

            <img class="image1 rellax img-fluid" data-rellax-percentage="0.5" data-rellax-speed="0.5"  src="<?php echo get_stylesheet_directory_uri(); ?>/images/about-circle1.png" alt=" Lawyer Circle Image">

            <img class="image2 wow fadeIn img-fluid"
            data-wow-duration="1s" src="<?php echo get_stylesheet_directory_uri(); ?>/images/about-circle2.png" alt=" Lawyer Circle Image">

            <img class="image3 wow fadeIn img-fluid"  data-wow-duration="1s"  src="<?php echo get_stylesheet_directory_uri(); ?>/images/about-circle3.png" alt="Circle Image">


            <img class="image4 wow zoomIn img-fluid" data-wow-delay="700" src="<?php the_field ('circle_image'); ?>" alt="Lawyer Circle Image">


          </div>

          <div class="process-wrap">
            <div class="how-we-work">
              <?php the_field ('how_we_work'); ?>
            </div>
          </div>
        </div>

        <div class="section-callouts">
          <div class="container-fluid">
            <div class="row align-items-center">

              <div class="col col-12 col-md-6">
                <div class="contact-callout-one contact-callout wow fadeInUp" data-wow-duration=".6s">

                  <?php
                    $verdicts = array(
                      'post_type' => 'testimonials',
                      'post_status' => 'publish',
                      'showposts' => 1
                    );
                    $loop = new WP_Query($verdicts);

                    if ($loop->have_posts()) : ?>
                    <?php while($loop->have_posts()) : $loop->the_post(); ?>
                      <div class="single-testimonial">
                      <div class="container">
                        <div class="row">
                          <div class="col col-12">
                            <div class="quote">
                              <h2><?php the_field ('quote'); ?></h2>
                            </div>
                          </div>
                          <div class="col col-12">
                            <div class="quote-author">
                              <?php the_field ('quote_author'); ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <?php endwhile; ?>
                    <?php endif; ?>
                  <?php wp_reset_postdata(); ?>


                </div>
              </div>

              <div class="col col-12 col-md-6">
                <div class="contact-callout-two contact-callout wow fadeInUp" data-wow-duration=".6s" data-wow-delay=".3s">

                <a href="<?php echo home_url( '/careers' ); ?>">
                  <div class="callout-wrapper">
                    <?php the_field ('callout_header'); ?><br>
                    <div class="orangeLine">
                  </div>
                </div></a>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
</main>
<?php get_footer(); ?>
