<?php

/* Template Name: personal injury */

get_header(); ?>
<main id="main" class="site-main index-main" role="main">
<div class="page-wrapper">
  <div class="container-wrapper">
      <div class="top-image-wrapper">

        <!-- image for mobile size only -->

            <img class="oval-small-screen wow fadeIn img-fluid"  data-wow-duration="1s" src="<?php echo get_stylesheet_directory_uri(); ?>/images/Pcollab.png" alt="circle stock">

        <!-- ends here -->

        <img class="oval1 oval-large wow fadeIn img-fluid"  data-wow-duration="1s" src="<?php echo get_stylesheet_directory_uri(); ?>/images/oval1.png" alt="">

        <img class="oval2 oval-large wow fadeIn img-fluid"  data-wow-duration="1s" data-wow-delay="1s" src="<?php echo get_stylesheet_directory_uri(); ?>/images/graycircle.png" alt="">

        <img class="oval3 oval-large wow fadeIn rellax img-fluid"  data-rellax-percentage="0.5" data-rellax-speed="-1"  data-wow-duration="1s" data-wow-delay="1s" src="<?php echo get_stylesheet_directory_uri(); ?>/images/oval3.png" alt="">


        <img class="oval4 oval-large wow fadeIn img-fluid" data-wow-duration="1s" data-wow-delay="1s" src="<?php echo get_stylesheet_directory_uri(); ?>/images/oval4.png" alt="">

        <img class="oval5 oval-large wow fadeInUp img-fluid" data-wow-duration=".6s" data-wow-delay="1s" src="<?php echo get_stylesheet_directory_uri(); ?>/images/oval5.png" alt="">

      </div>

  <img class="oval6 wow fadeInUp" data-wow-duration=".6s" data-wow-delay="1s" src="<?php echo get_stylesheet_directory_uri(); ?>/images/oval33.png" alt="">

  </div>
  <div class="container">
    <div class="row">
      <div class="col col-12">
        <div class="top-content text-center mx-auto wow fadeIn" data-wow-duration="1s" data-wow-delay=".6s">
          <div class="breadcrumbs">
            <?php  echo do_shortcode( '[breadcrumb]' ); ?>
          </div>
            <?php the_field ('top_content_category'); ?>
            <div class="button-wrapper">
                <a class="goldberg-button button-middle text-center" href="<?php echo home_url( '/contact-us' ); ?>"><?php the_field('button_consultation', 'option'); ?></a>
            </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col col-12">
        <div class="practice-history text-center wow fadeIn" data-wow-duration="1.2s" data-wow-delay=".2s">
            <?php the_field ('practice_history_content'); ?>
        </div>
      </div>
    </div>
  </div>

    <div class="all-practice-list wow fadeIn" data-wow-duration="1s">
      <div class="container">
        <div class="list-wrap">
        <div class="row">
            <div class="col">
              <!-- <div class="row"> -->

                <ul class="practice-links">
                    <?php
                    global $id;
                    wp_list_pages( array(
                        'title_li'    => '',
                        'child_of'    => $post->ID,
                        'show_date'   => 'modified',
                        'date_format' => $date_format
                    ) );
                    ?>
                </ul>

              <!-- </div> -->
            </div>
          </div>
          </div>
        </div>
      </div>

    <div class="section-center">

      <img class="backPattern wow fadeIn img-fluid"  data-wow-duration="2s" src="<?php echo get_stylesheet_directory_uri(); ?>/images/categoryBack.jpg" alt="background pattern">



      <div class="container">
        <div class="practice-lawyers-wrap wow fadeIn" data-wow-duration="2s">
          <div class="row align-items-center">
          <div class="col col-12 col-md-5">
            <div class="practice-area-content">
                <?php the_field ('practice_lawyer_content'); ?>
                <a href="<?php echo home_url( '/attorneys' ); ?>">View all attorneys
                  <div class="orangeLine"></div>
                </a>
            </div>
          </div>

          <div class="col col-12 col-md-6">
            <div class="single-attorney-wrap">
            <?php
              $post = array(
                // 'name'  => $the_slug,
                'post_type' => 'attorneys',
                'post_status' => 'publish',
                'order' => 'ASC',
                'showposts' => 8
              );
              $loop = new WP_Query($post);

              if ($loop->have_posts()) : ?>
              <?php while($loop->have_posts()) : $loop->the_post(); ?>

                <div class="image-wrap">
                  <a class="wrap-link" href="<?php the_permalink(); ?>"
                    title="<?php the_title_attribute(); ?>">
                    <img src="<?php echo get_field('profile_image'); ?>" alt="">
                    <div class="title">
                    <?php the_title(); ?>
                    </div>
                  </a>
                </div>

              <?php endwhile; shuffle($args); ?>

              <?php endif; ?>
            <?php wp_reset_postdata(); ?>
            </div>
          </div>
        </div>

        </div>

      </div>


      <div class="container">

      <div class="row">
        <div class="quilt-box wow fadeIn" data-wow-duration="1s">
          <div class="container container-quilt">
            <div class="row no-gutters">
              <div class="col col-12 col-lg-6 column1">


                <?php
                  $verdicts = array(
                    'post_type' => 'testimonials',
                    'post_status' => 'publish',
                    'orderby' => 'date',
                    'showposts' => 3
                  );
                  $loop = new WP_Query($verdicts);

                  if ($loop->have_posts()) : ?>
                  <div id="testslide" class="single-testimonial test-slide">
                  <?php while($loop->have_posts()) : $loop->the_post(); ?>
                    <div class="container">
                      <div class="row align-items-center">
                        <div class="col col-12">
                          <div class="quote">
                            <h2><?php the_field ('quote'); ?></h2>
                          </div>
                        </div>
                        <div class="col col-12">
                          <div class="quote-author">
                            <?php the_field ('quote_author'); ?>
                          </div>
                        </div>
                      </div>
                    </div>


                  <?php endwhile; ?>
                  </div>

                  <?php endif; ?>
                <?php wp_reset_postdata(); ?>

              </div>
              <div class="col col-12 col-lg-6 column2">
                <div class="slide-wrap">


                <?php
                  $post = array(
                    'post_type' => 'post',
                    'post_status' => 'publish',
                    'order' => 'DSC',
                    'showposts' => 3
                  );
                  $loop = new WP_Query($post);

                  if ($loop->have_posts()) : ?>
                  <?php while($loop->have_posts()) : $loop->the_post(); ?>

                    <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );?>

                    <div class="single-slide" style="background-image: url('<?php echo $backgroundImg[0]; ?>')">

                        <div class="post-meta">
                          <div class="post-date"><?php the_time('F j, Y'); ?></div>
                          <a class="postH" href="<?php echo get_permalink(); ?>">
                          <div class="post-title"><?php the_title(); ?></div></a>
                          <div class="author">Katie Hubbard</div>
                        </div>
                          <img class="gradient" src="<?php echo get_stylesheet_directory_uri(); ?>/images/gradientFeatured2.png" alt="">
                    </div>

                  <?php endwhile; shuffle($args); ?>

                  <?php endif; ?>
                <?php wp_reset_postdata(); ?>
              </div>
              </div>

            </div>

            <div class="row no-gutters">
              <div class="col col-12 col-lg-6 column3">

                <?php
                  $verdicts = array(
                    'post_type' => 'settlements_verdicts',
                    'post_status' => 'publish',
                    'orderby' => 'post_date',
                    'showposts' => 1
                  );
                  $loop = new WP_Query($verdicts);

                  if ($loop->have_posts()) : ?>
                  <?php while($loop->have_posts()) : $loop->the_post(); ?>
                    <div class="single-settlement wow fadeIn" data-wow-duration="1s" data-wow-delay=".5s">
                      <div class="container-fluid">
                        <div class="row align-items-center">
                          <div class="col col-12 col-lg-4">
                          <h1 class="verdict-date"><?php echo get_field('verdict_date'); ?></h1>

                          </div>
                          <div class="col-12 col-lg-8">
                              <h2><?php the_field('quilt_box_three_header', 'option'); ?></h2>
                            <div class="verdict-content">
                               <?php echo get_field('verdict_content'); ?>
                            </div>
                            <div class="button-wrap">
                              <a href="#"><?php the_field('button_view', 'option'); ?>
                                <div class="orangeLine"></div>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                  <?php endwhile; shuffle($args); ?>
                  <?php endif; ?>
                <?php wp_reset_postdata(); ?>


              </div>
              <div class="col col-12 col-lg-6 column4">
                <div class="control-wrap">
                    <?php the_field('fourth_quilt_box_callout', 'option'); ?>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
      </div>
     </div>
    </div>
  </div>

</div>
</main>
<script>
// Accepts any class name
var rellax = new Rellax('.rellax');

</script>

<?php get_footer(); ?>
