<?php

/* Template Name: contact us */

get_header(); ?>
<main id="main" class="site-main index-main" role="main">
<div class="page-wrapper">

  <div class="contact-section">

    <div class="contact-form-wrapper">

      <div class="container conX">

        <div class="row">

          <div class="col col-12 col-md-9 contactBack">
            <div class="top-contact-content wow fadeIn" data-wow-duration="1s">
                <?php the_field ('top_contact_content'); ?>
            </div>
            <div class="gravityform-wrapper wow fadeIn" data-wow-duration="2s">
                <?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true"]'); ?>
            </div>
          </div>

          <div class="col col-12 col-md-3">
            <div class="grayBack">

            <div class="address-wrapper wow fadeIn" data-wow-duration="1s">
              <p class="firm-location"><?php the_field ('firm_address'); ?></p>
              <div class="social-icons-row">
                <ul class="contact-social">
                    <li><a href="#"><img class="social-logo facebookLogo" src="<?php echo get_stylesheet_directory_uri(); ?>/svg/facebook-logo.svg"  alt="Facebook Logo"></a></li>
                    <li><a href="#"><img class="social-logo linkedinLogo" src="<?php echo get_stylesheet_directory_uri(); ?>/svg/linkedin-logo.svg" alt="LinkedIn Logo"></a></li>
                    <li><a href="#"><img class="social-logo twitterLogo" src="<?php echo get_stylesheet_directory_uri(); ?>/svg/twitter-logo.svg" alt="Twitter Logo"></a></li>
                </ul>
              </div>

            </div>

          </div>
        </div>

        </div>

      </div>

    </div>

  </div>


  <div class="section-callouts">
    <div class="container-fluid">
      <div class="row">

        <div class="col col-12 col-md-6">
          <div class="contact-callout-one contact-callout wow fadeInUp" data-wow-duration=".6s">

            <?php
              $verdicts = array(
                'post_type' => 'testimonials',
                'post_status' => 'publish',
                'showposts' => 1
              );
              $loop = new WP_Query($verdicts);

              if ($loop->have_posts()) : ?>
              <?php while($loop->have_posts()) : $loop->the_post(); ?>
                <div class="single-testimonial">
                <div class="container">
                  <div class="row align-items-center">
                    <div class="col col-12">
                      <div class="quote">
                        <h2><?php the_field ('quote'); ?></h2>
                      </div>
                    </div>
                    <div class="col col-12">
                      <div class="quote-author">
                        <?php the_field ('quote_author'); ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <?php endwhile; ?>
              <?php endif; ?>
            <?php wp_reset_postdata(); ?>


          </div>
        </div>

        <div class="col col-12 col-md-6">
          <div class="contact-callout-two contact-callout wow fadeInUp" data-wow-duration=".6s" data-wow-delay=".3s">

          <a href="<?php echo home_url( '/careers' ); ?>">
            <div class="callout-wrapper">
              <?php the_field ('callout_text'); ?><br>
              <div class="orangeLine">
            </div>
          </div></a>


          </div>
        </div>

      </div>

    </div>

  </div>

</div>
</main>







<?php get_footer(); ?>
