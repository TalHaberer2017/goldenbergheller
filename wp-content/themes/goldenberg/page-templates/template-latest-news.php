<?php

/* Template Name: Latetst News */

get_header(); ?>
<main id="main" class="site-main index-main" role="main">
  <div class="page-wrapper">
    <div class="top-header text-center">
    <h3><?php the_field ('top_header'); ?></h3>
  </div>


  <div class="container">
    <div class="news-wrap">

            <?php
              $articles  = array(
                'post_type' => 'articles',
                'post_status' => 'publish',
                'showposts' => -1
              );
              $loop = new WP_Query($articles  );

              if ($loop->have_posts()) : ?>
              <?php while($loop->have_posts()) : $loop->the_post(); ?>
                <div class="single-news">
                      <p class="icon-date"><?php echo get_the_date( get_option('date_format') ); ?></p>
                      <div>
                      <div class="news-title">
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                      </div>
                      </div>

              </div>

              <?php endwhile;

              // Previous/next page navigation.
              the_posts_pagination( array(
                'prev_text'          => __( 'Previous', 'basetheme' ),
                'next_text'          => __( 'Next', 'basetheme' ),
                'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'basetheme' ) . ' </span>',
              ) );


               ?>
              <?php endif; ?>
            <?php wp_reset_postdata(); ?>


  </div>
  </div>
  </div>
</main>
  <?php get_footer(); ?>
