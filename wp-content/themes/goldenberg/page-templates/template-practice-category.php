<?php

/* Template Name: practice category */

get_header(); ?>
<main id="main" class="site-main index-main" role="main">
<div class="page-wrapper">
  <?php while ( have_posts() ) : the_post(); ?>
  <div class="container-wrapper">
      <div class="top-image-wrapper">


        <img class="oval1 oval-large wow zoomIn img-fluid"  data-wow-delay="900" src="<?php the_field ('circle_image'); ?>" alt="Goldenberg & Heller Circle Image">

        <img class="oval2 oval-large wow fadeIn img-fluid"  data-wow-delay="800" src="<?php echo get_stylesheet_directory_uri(); ?>/images/graycircle.png" alt="Goldenberg & Heller Circle Image">

        <img class="oval3 oval-large rellax img-fluid"  data-rellax-percentage="0.5" data-rellax-speed="-1" src="<?php echo get_stylesheet_directory_uri(); ?>/images/oval3.png" alt="Goldenberg & Heller Circle Image">


        <img class="oval4 oval-large wow fadeIn img-fluid" data-wow-delay="700" src="<?php echo get_stylesheet_directory_uri(); ?>/images/oval4.png" alt="Goldenberg & Heller Circle Image">

        <img class="oval5 oval-large wow fadeInUp img-fluid" data-wow-delay="500" src="<?php echo get_stylesheet_directory_uri(); ?>/images/oval5.png" alt="Goldenberg & Heller Circle Image">

      </div>

  <img class="oval6 wow fadeInUp" data-wow-duration=".6s" data-wow-delay="1s" src="<?php echo get_stylesheet_directory_uri(); ?>/images/oval33.png" alt="Goldenberg & Heller Circle Image">

  </div>
  <div class="container">
    <div class="row">
      <div class="col col-12">
        <div class="top-content text-center mx-auto wow fadeIn" data-wow-duration="1s" data-wow-delay=".6s">
          <div class="breadcrumbs">
            <?php  echo do_shortcode( '[breadcrumb]' ); ?>
          </div>
            <?php the_field ('top_content_category'); ?>
            <div class="button-wrapper">
                <a class="goldberg-button button-middle text-center" href="<?php echo home_url( '/contact-goldenberg-heller-antognoli/' ); ?>"><?php the_field('button_consultation', 'option'); ?></a>
            </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col col-12">
        <div class="practice-history wow fadeIn" data-wow-duration="1.2s" data-wow-delay=".2s">
            <?php the_field ('practice_history_content'); ?>
        </div>
      </div>
    </div>
  </div>

    <div class="practice-rules wow fadeIn" data-wow-duration="1s">
      <div class="container">
        <div class="list-wrap">

                <ul class="practice-links">
                    <?php
                    global $post;
                    wp_list_pages( array(
                        'title_li'    => '',
                        'child_of'    => $post->ID,
                    ) );
                    ?>
                </ul>

          </div>
        </div>
      </div>

    <div class="section-center">

      <img class="backPattern wow fadeIn img-fluid"  data-wow-duration="2s" src="<?php echo get_stylesheet_directory_uri(); ?>/images/categoryBack.jpg" alt="Goldenberg & Heller Background Pattern">



      <div class="container-fluid">
        <div class="practice-lawyers-wrap wow fadeIn" data-wow-duration="2s">
          <div class="row align-items-center">
          <div class="col col-12 col-md-5">
            <div class="practice-area-content">
                <?php the_field ('practice_lawyer_content'); ?>
                <a href="<?php echo home_url( '/our-attorneys/' ); ?>">View all attorneys
                  <div class="orangeLine"></div>
                </a>
            </div>
          </div>

          <div class="col col-12 col-md-6">
            <div class="single-attorney-wrap">

              <?php

              $posts = get_field('category_lawyers');

              if( $posts ): ?>
                  <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                      <?php setup_postdata($post); ?>
                      <div class="image-wrap">
                          <a class="wrap-link" href="<?php the_permalink(); ?>">  <span><img src="<?php the_field('profile_image'); ?>" alt="Goldenberg & Heller Attorney Image"></span><?php the_title(); ?></a>
                      </div>
                  <?php endforeach; ?>
                  <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
              <?php endif; ?>

            </div>
          </div>
        </div>

        </div>

      </div>


      <div class="container">

      <div class="row">
        <div class="quilt-box wow fadeIn" data-wow-duration="1s">
          <div class="container container-quilt">
            <div class="row no-gutters">
              <div class="col col-12 col-lg-6 column1">


                <?php
                  $verdicts = array(
                    'post_type' => 'testimonials',
                    'post_status' => 'publish',
                    'orderby' => 'date',
                    'showposts' => 3
                  );
                  $loop = new WP_Query($verdicts);

                  if ($loop->have_posts()) : ?>
                  <div id="testslide" class="single-testimonial test-slide">
                  <?php while($loop->have_posts()) : $loop->the_post(); ?>
                    <div class="container">
                      <div class="row">
                        <div class="col col-12 align-self-center">
                          <div class="quote align-middle">
                            <h2><?php the_field ('quote'); ?></h2>
                          </div>
                        </div>
                        <div class="col col-12">
                          <div class="quote-author">
                            <?php the_field ('quote_author'); ?>
                          </div>
                        </div>
                      </div>
                    </div>


                  <?php endwhile; ?>
                  </div>

                  <?php endif; ?>
                <?php wp_reset_postdata(); ?>

              </div>
              <div class="col col-12 col-lg-6 column2">
                <div class="slide-wrap">


                <?php
                  $args = array(
                    'post_type' => 'post',
                    'post_status' => 'publish',
                    'order' => 'DSC',
                    'showposts' => 3
                  );
                  $loop = new WP_Query($args);

                  if ($loop->have_posts()) : ?>
                  <?php while($loop->have_posts()) : $loop->the_post(); ?>

                    <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );?>

                    <div class="single-slide" style="background-image: url('<?php echo $backgroundImg[0]; ?>')">

                        <div class="post-meta">
                          <div class="post-date"><?php the_time('F j, Y'); ?></div>
                          <a class="postH" href="<?php echo get_permalink(); ?>">
                          <div class="post-title"><?php the_title(); ?></div></a>
                          <div class="row align-items-center row-author">
                            <div class="col col-3 col-md-2 col-lg-2">
                              <div class="person">
                                <?php echo get_avatar( get_the_author_meta( 'ID' ) ) ?>
                              </div>
                            </div>
                            <div class="col col-6 col-md-6 col-lg-6">
                              <p class="authorName"> <?php the_author(); ?> </p>
                            </div>
                          </div>
                        </div>
                          <img class="gradient" src="<?php echo get_stylesheet_directory_uri(); ?>/images/gradientFeatured2.png" alt="Goldenberg & Heller Gradient">
                    </div>

                  <?php endwhile; ?>

                  <?php endif; ?>
                <?php wp_reset_postdata(); ?>
              </div>
              </div>

            </div>

            <div class="row no-gutters">
              <div class="col col-12 col-lg-6 column3">

                <?php
                  $verdicts = array(
                    'post_type' => 'settlements_verdicts',
                    'post_status' => 'publish',
                    'orderby' => 'post_date',
                    'showposts' => 1
                  );
                  $loop = new WP_Query($verdicts);

                  if ($loop->have_posts()) : ?>
                  <?php while($loop->have_posts()) : $loop->the_post(); ?>
                    <div class="single-settlement wow fadeIn" data-wow-duration="1s">
                      <div class="container-fluid">
                        <div class="row align-items-center">
                          <div class="col col-12 col-lg-4">
                          <h1 class="verdict-date"><?php echo get_field('verdict_date'); ?></h1>

                          </div>
                          <div class="col-12 col-lg-8">
                              <h2><?php the_field('quilt_box_three_header', 'option'); ?></h2>
                            <div class="verdict-content">
                               <?php echo get_field('verdict_content'); ?>
                            </div>
                            <div class="button-wrap">
                              <a href="<?php echo home_url( '/notable-settlements-and-verdicts' ); ?>"><?php the_field('button_view', 'option'); ?>
                                <div class="orangeLine"></div>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                  <?php endwhile; shuffle($args); ?>
                  <?php endif; ?>
                <?php wp_reset_postdata(); ?>


              </div>
              <div class="col col-12 col-lg-6 column4">
                <div class="control-wrap">
                    <?php the_field('fourth_quilt_box_callout', 'option'); ?>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
      </div>
    </div>
     <?php endwhile; ?>
    </div>
</main>
<?php get_footer(); ?>
