<?php
/* Template Name: News and Insights */

get_header(); ?>
<main id="main" class="site-main index-main" role="main">
<div class="page-wrapper">

  <div class="top-content wow fadeIn" data-wow-duration="1s" data-wow-delay=".1s">
    <div class="container">

    <h3><?php the_field ('page_title'); ?></h3>

    <?php

      $posts = get_posts(array(
        'showposts' => 1,
      	'meta_query' => array(
      		array(
      			'key' => 'enable_featured_post',
      			'compare' => '==',
      			'value' => '1'
      		)
      	)
      ));

      if( $posts ): ?>

      	<div>

      	<?php foreach( $posts as $post ):

      		setup_postdata( $post )

      		?>
      		<div class="featured-post">
            <div class="post-image"><?php the_post_thumbnail(); ?></div>
            <div class="post-meta">
              <div class="row">
                <div class="col col-12">
                  <p class="post-date"><?php the_time('F j, Y'); ?></p>
                  <a class="postH" href="<?php the_permalink(); ?>"><h1 class="post-title"><?php the_title(); ?></h1></a>
                  <div class="post-categories"><?php the_category(' | '); ?></div>
                </div>
              </div>
              <div class="row align-items-center row-author">
                <div class="col col-3 col-md-2">
                  <div class="author">
                    <?php echo get_avatar( get_the_author_meta( 'ID' ) ) ?>
                  </div>
                </div>
                <div class="col col-6 col-md-6">
                  <p class="authorName"> <?php the_author(); ?> </p>
                </div>

              </div>
            </div>
      		</div>

      	<?php endforeach; ?>

      </div>

      	<?php wp_reset_postdata(); ?>

      <?php endif; ?>

  </div>
</div>

    <div class="blog-section">
      <div class="container">
        <div class="row">
          <div class="col col-12 col-lg-5 columnSwitch1">
            <div class="blog-list-wrapper">

              <?php
                function bt_featured_post_ids($num = 1) {
                	$post_ids = array();

                	$args = array('post_type' => 'post',
                				'post_status' => 'publish',
                				'showposts' => $num,
                				'orderby' => 'date',
                				'order' => 'desc',
                				'meta_query' => array(
                					'key' => 'enable_featured_post',
                					'value' => 1,
                					'compare' => '=='
                				)
                			);

                	$c_query = new WP_Query($args);

                	if ($c_query->have_posts()) {
                		while ($c_query->have_posts()) {
                			$c_query->the_post();
                			$post_ids[] = get_the_ID();
                		}
                		wp_reset_postdata();

                	}

                	return $post_ids;
                } ?>

              <?php
                $post = array(
                  'post_type' => 'post',
                  'post_status' => 'publish',
                  'orderby' => 'DESC',
                  'offset'  => 1,
                  'post__not_in' => array( 1, ),
                  'showposts' => 2
                );
                $loop = new WP_Query($post);

                if ($loop->have_posts()) : ?>
                <?php while($loop->have_posts()) : $loop->the_post(); ?>
                  <div class="single-post">
                    <div class="person">
                      <?php echo get_avatar( get_the_author_meta( 'ID' ) ) ?>
                    </div>
                    <div class="image">
                      <?php the_post_thumbnail(); ?>
                    </div>
                    <div class="row">
                      <div class="post-meta">
                        <div class="post-date"><?php the_time('F j, Y'); ?></div>
                        <a class="postH" href="<?php echo get_permalink(); ?>">
                        <div class="post-title"><?php the_title(); ?></div></a>
                        <div class="post-categories"><?php the_category(' | '); ?></div>
                      </div>
                    </div>
                  </div>

                <?php endwhile; ?>

                <?php endif; ?>
              <?php wp_reset_postdata(); ?>

            </div>

          </div>
          <div class="col col-12 col-lg-7 columnSwitch2">
            <div class="row">
              <div class="categories-wrap">
              <?php get_sidebar(); ?>

              </div>

            </div>
            <div class="row">
              <div class="latest-news-wrap wow">
                <h2><?php the_field ('box_two_header'); ?></h2>

                <ul class="articles-wrap">

                  <?php
                    $articles = array(
                      'post_type' => 'articles',
                      'post_status' => 'publish',
                      'orderby' => 'post_date',
                      'order' => 'DSC',
                      'showposts' => 5
                    );
                    $loop = new WP_Query($articles);

                    if ($loop->have_posts()) : ?>
                    <?php while($loop->have_posts()) : $loop->the_post(); ?>

                    <li class="single-link">
                      <a class="news-link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </li>

                    <?php endwhile; shuffle($args); ?>
                    <?php endif; ?>
                  <?php wp_reset_postdata(); ?>


                </ul>

                <a class="view-all" href="<?php echo home_url( '/latest-news' ); ?>">
                  <div class="callout-wrapper">
                    <?php the_field ('link_text'); ?><br>
                    <div class="orangeLine">
                  </div>
                </div></a>

                <div class="social-icons-row">

                  <ul class="social-icons-row">
                    <li><a href="<?php the_field('facebook_link', 'option'); ?>" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/facebook3.png" alt="Facebook Logo"></a></li>
                    <li><a href="<?php the_field('linkedin_link', 'option'); ?>" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/linkedin3.png" alt="LinkedIn Logo"></a></li>
                    <li><a href="<?php the_field('twitter_link', 'option'); ?>" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/twitter3.png" alt="Twitter Logo"></a></li>

                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>


      <div class="blog-section-two">
        <div class="container">
          <div class="blog-list-wrapper list-two">


            <?php
              $post = array(
                'post_type' => 'post',
                'post_status' => 'publish',
                'orderby' => 'DESC',
                'offset'  => 3,
                'exclude' => $num,
                'showposts' => 6
              );
              $loop = new WP_Query($post);

              if ($loop->have_posts()) : ?>
              <?php while($loop->have_posts()) : $loop->the_post(); ?>
                <div class="single-post">
                  <div class="person">
                    <?php echo get_avatar( get_the_author_meta( 'ID' ) ) ?>
                  </div>
                  <div class="image">
                    <?php the_post_thumbnail(); ?>
                  </div>
                  <div class="row">
                    <div class="post-meta">
                      <div class="post-date"><?php the_time('F j, Y'); ?></div>
                      <a class="postH" href="<?php echo get_permalink(); ?>">
                      <div class="post-title"><?php the_title(); ?></div></a>
                      <div class="post-categories"><?php the_category(' | '); ?></div>
                    </div>
                  </div>
                </div>

              <?php endwhile; ?>

              <?php endif; ?>
            <?php wp_reset_postdata(); ?>
</div>

      		<?php wp_reset_postdata(); ?>

          <a class="more-button text-center mx-auto d-block" href="<?php echo home_url( '/all-insights/' ); ?>">Load all posts</a>
        </div>
      </div>


    <div class="verdict-slider-section">
      <div class="container">

      <div>
        <h2><?php the_field ('slider_heading'); ?></h2>

        <div class="verdict-slider">


        <?php
          $verdicts = array(
            'post_type' => 'settlements_verdicts',
            'post_status' => 'publish',
            'showposts' => 5
          );
          $loop = new WP_Query($verdicts);

          if ($loop->have_posts()) : ?>
          <?php while($loop->have_posts()) : $loop->the_post(); ?>
            <div class="single-verdict-slide">
            <div class="container-fluid">
              <div class="row align-items-center">
                <div class="col col-12 col-sm-4">
                  <div class="verdict-date">
                    <h1><?php the_field ('verdict_date'); ?></h1>
                  </div>
                </div>
                <div class="col col-12 col-sm-8">
                  <div class="verdict-content">
                    <?php the_field ('verdict_content'); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <?php endwhile; ?>
          <?php endif; ?>
        <?php wp_reset_postdata(); ?>
      </div>

        <div class="view-all">
          <a href="<?php echo home_url( '/notable-settlements-and-verdicts' ); ?>"><?php the_field('button_view', 'option'); ?>
            <div class="orangeLine"></div>
          </a>
        </div>

      </div>

    </div>
    <div class="verdict-slider-box">

    </div>

  </div>

  </div>
</main>
<?php get_footer(); ?>
