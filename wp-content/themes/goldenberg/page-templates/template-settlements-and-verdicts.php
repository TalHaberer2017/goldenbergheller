<?php

/* Template Name: settlements  */

get_header(); ?>
<main id="main" class="site-main index-main" role="main">
  <div class="page-wrapper">
    <div class="top-header">
    <?php the_field ('top_header_field'); ?>
  </div>


  <div class="container">
    <div class="news-wrap">

            <?php
              $verdicts = array(
                'post_type' => 'settlements_verdicts',
                'post_status' => 'publish',
                'showposts' => -1
              );
              $loop = new WP_Query($verdicts);

              if ($loop->have_posts()) : ?>
              <?php while($loop->have_posts()) : $loop->the_post(); ?>
                <div class="single-verdict">
                    <div class="verdict-wrap">
                      <!-- <div class="verdict-date">
                        <h2><?php the_field ('verdict_date'); ?></h2>
                      </div> -->
                      <div>
                        <p class="verdict-content"><?php the_field ('verdict_content'); ?></p>
                      </div>
                    </div>
              </div>

              <?php endwhile; ?>
              <?php endif; ?>
            <?php wp_reset_postdata(); ?>


  </div>
  </div>
  </div>
</main>

  <?php get_footer(); ?>
