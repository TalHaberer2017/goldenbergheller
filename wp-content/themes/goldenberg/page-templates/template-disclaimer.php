<?php

/* Template Name: disclaimer */

get_header(); ?>
<main id="main" class="site-main index-main" role="main">
<div class="page-wrapper">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="disclaimer-wrap mx-auto">

              <?php the_content(); ?>

        </div>
      </div>
    </div>
  </div>
</div>
</main>





<?php get_footer(); ?>
