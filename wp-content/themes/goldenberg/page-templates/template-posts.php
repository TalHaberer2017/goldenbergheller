<?php
/* Template Name: all posts */

get_header(); ?>

<main id="main" class="site-main index-main" role="main">
<div class="page-wrapper">

<?php if ( have_posts() ) : the_post(); ?>
  <div class="container">
    <div class="entry-header">
      <h3 class="text-center">News & Insights</h3>
    </div>
    <div class="row">
      <div class="col col-12 col-lg-12">
        <div class="blog-content-wrap wow fadeIn" data-wow-duration="1s">

            <div class="blog-list-wrapper list-two">


              <?php // Display blog posts on any page @ https://m0n.co/l
              $temp = $wp_query; $wp_query= null;
              $wp_query = new WP_Query(); $wp_query->query('posts_per_page=9' . '&paged='.$paged);
              while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

              <div class="single-post">
                <div class="person">
                  <?php echo get_avatar( get_the_author_meta( 'ID' ) ) ?>
                </div>
                <div class="image">
                  <?php the_post_thumbnail(); ?>
                </div>
                <div class="row">
                  <div class="post-meta">
                    <div class="post-date"><?php the_time('F j, Y'); ?></div>
                    <a class="postH" href="<?php echo get_permalink(); ?>">
                    <div class="post-title"><?php the_title(); ?></div></a>
                    <div class="post-categories"><?php the_category(' | '); ?></div>
                  </div>
                </div>
              </div>

              <?php endwhile; ?>
    </div>


              <?php wp_reset_postdata(); ?>

            </div>
          </div>


      <hr>


      <!-- <div class="col col-12 col-lg-3">
        <div class="sidebar-wrap">
            <?php get_sidebar(); ?>
        </div>
        <div class="newsletter-wrap">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/svg/signupLogo.svg" alt="">
          <h1>Keep up with GHA</h1>
          <p>Sign up for our newsletter</p>
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/orangeLine.png" alt="">
          <?php echo do_shortcode('[gravityform id="2" title="false" description="false" ajax="true"]'); ?>
        </div>
      </div> -->

    </div>
  <?php if ( is_home() && ! is_front_page() ) : ?>
    <header>
      <h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
    </header>
  <?php endif; ?>

  <?php
  // Start the loop.
  while ( have_posts() ) : the_post();

    /*
     * Include the Post-Format-specific template for the content.
     * If you want to override this in a child theme, then include a file
     * called content-___.php (where ___ is the Post Format name) and that will be used instead.
     */
    // get_template_part( 'template-parts/content', get_post_format() );

  // End the loop.
  endwhile;

  // Previous/next page navigation.
  the_posts_pagination( array(
    'prev_text'          => __( 'Prev', 'basetheme' ),
    'next_text'          => __( 'Next', 'basetheme' ),
    'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'basetheme' ) . ' </span>',
  ) );

// If no content, include the "No posts found" template.
else :
  get_template_part( 'template-parts/content', 'none' );

endif;
?>

<?php get_sidebar(); ?>
  </div>
</div>
</main><!-- .site-main -->

<?php get_footer(); ?>
