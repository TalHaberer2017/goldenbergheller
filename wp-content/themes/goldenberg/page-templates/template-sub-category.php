<?php

/* Template Name: Practice Sub Category */

get_header(); ?>

<main id="main" class="site-main index-main" role="main">
<div class="page-wrapper">

  <div class="top-content">
    <div class="container">
      <?php  echo do_shortcode( '[breadcrumb]' ); ?>
          <div class="content-wrap wow fadeIn" data-wow-duration="1s">
            <?php the_field ('top_page_content'); ?>
          </div>

          <div class="button-wrap">

            <a class="goldberg-button mx-auto wow fadeIn" data-wow-duration="1s" data-wow-delay=".5s" href="<?php echo home_url( '/contact-goldenberg-heller-antognoli/' ); ?>"><?php the_field('button_consultation', 'option'); ?></a>
          </div>
      </div>
    </div>


    <div class="practice-approach">
      <div class="container">
        <div class="row">
          <div class="col col-12">
            <h2 class="approachHeader wow fadeIn" data-wow-delay=".5s" data-wow-duration="1s"><?php the_field ('our_approach_header'); ?></h2>
          </div>
        </div>
        <div class="row">
          <div class="col col-12">
            <div class="white-box-circumstances wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">

              <?php the_field ('white_box_circumstances'); ?>

            </div>
          </div>
        </div>
        <div class="row">
          <div class="col col-12 col-md-9">
            <div class="our-approach-content wow fadeIn" data-wow-duration="1s">

              <?php the_field ('our_approach_content'); ?>

            </div>
          </div>
        </div>
      </div>

    </div>

    <div class="information-callout wow fadeInUp" data-wow-duration="1s">

      <div class="container">
        <div class="row">
          <div class="col col-12 col-lg-4">
            <div class="image-wrap">

              <div class="sub-circle1">
                <img class="img-fluid" src="<?php the_field ('box_image'); ?>" alt="Goldenberg & Heller Circle Image">

              </div>


              <img class="sub-circle2 rellax img-fluid" data-rellax-percentage="0.5" data-rellax-speed="0.6" src="<?php echo get_stylesheet_directory_uri(); ?>/images/sub-circle2.png" alt="Goldenberg & Heller Circle Image">

              <img class="sub-circle3 img-fluid rellax" data-rellax-percentage="0.5" data-rellax-speed="-0.6" src="<?php echo get_stylesheet_directory_uri(); ?>/images/sub-circle3.png" alt="Goldenberg & Heller Circle Image">

           </div>

          </div>

          <div class="col col-12 col-lg-8">

            <div class="information-wrapper">
              <div class="row">
                <div class="col col-12">
                  <h1><?php the_field ('gray_box_header'); ?></h1>
                </div>
              </div>
              <div class="row">
                <div class="col col-12">
                  <div class="gray-box-content"><?php the_field ('gray_box_content'); ?></div>
                  <div class="button-wrap2">

                    <a class="goldberg-button" href="<?php echo home_url( '/contact-goldenberg-heller-antognoli/' ); ?>"><?php the_field('button_consultation', 'option'); ?></a>

                  </div>
                </div>
              </div>


            </div>
            <!-- <div class="row">
              <div class="button-wrap2">

                <a class="goldberg-button" href="<?php echo home_url( '/contact-us' ); ?>"><?php the_field('button_consultation', 'option'); ?></a>

              </div> -->

            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container">

    <div class="row">
      <div class="quilt-box wow fadeIn" data-wow-duration="1s">
        <div class="container container-quilt">
          <div class="row no-gutters">
            <div class="col col-12 col-lg-6 column1">


              <?php
                $verdicts = array(
                  'post_type' => 'testimonials',
                  'post_status' => 'publish',
                  'showposts' => 3
                );
                $loop = new WP_Query($verdicts);

                if ($loop->have_posts()) : ?>
                <div id="testslide" class="single-testimonial test-slide">
                <?php while($loop->have_posts()) : $loop->the_post(); ?>
                  <div class="container">
                    <div class="row">
                      <div class="col col-12">
                        <div class="quote">
                          <h2><?php the_field ('quote'); ?></h2>
                        </div>
                      </div>
                      <div class="col col-12">
                        <div class="quote-author">
                          <?php the_field ('quote_author'); ?>
                        </div>
                      </div>
                    </div>
                  </div>


                <?php endwhile; ?>
                </div>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>
            </div>

            <div class="col col-12 col-lg-6 column2">

              <div class="slide-wrap">


              <?php
                $post = array(
                  'post_type' => 'post',
                  'post_status' => 'publish',
                  'order' => 'DSC',
                  'showposts' => 3
                );
                $loop = new WP_Query($post);

                if ($loop->have_posts()) : ?>
                <?php while($loop->have_posts()) : $loop->the_post(); ?>

                  <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );?>

                  <div class="single-slide" style="background-image: url('<?php echo $backgroundImg[0]; ?>');">

                      <div class="post-meta">
                        <div class="post-date"><?php the_time('F j, Y'); ?></div>
                        <a class="postH" href="<?php echo get_permalink(); ?>">
                        <div class="post-title"><?php the_title(); ?></div></a>
                        <img src="" alt="Goldenberg & Heller Image">
                        <!-- <div class="author">Katie Hubbard</div> -->
                        <div class="row align-items-center row-author">
                          <div class="col col-3 col-md-2 col-lg-2">
                            <div class="person">
                              <?php echo get_avatar( get_the_author_meta( 'ID' ) ) ?>
                            </div>
                          </div>
                          <div class="col col-6 col-md-6 col-lg-6">
                            <p class="authorName"> <?php the_author(); ?> </p>
                          </div>
                        </div>
                      </div>
                        <img class="gradient" src="<?php echo get_stylesheet_directory_uri(); ?>/images/gradientFeatured2.png" alt="Goldenberg & Heller Image">
                  </div>

                <?php endwhile; shuffle($args); ?>

              </div>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>

            </div>

          </div>

          <div class="row no-gutters">
            <div class="col col-12 col-lg-6 column3">

              <?php
                $verdicts = array(
                  'post_type' => 'settlements_verdicts',
                  'post_status' => 'publish',
                  'orderby' => 'post_date',
                  'showposts' => 1
                );
                $loop = new WP_Query($verdicts);

                if ($loop->have_posts()) : ?>
                <?php while($loop->have_posts()) : $loop->the_post(); ?>
                  <div class="single-settlement">
                    <div class="container-fluid">
                      <div class="row align-items-center">
                        <div class="col col-12 col-lg-4">
                        <h1 class="verdict-date"><?php echo get_field('verdict_date'); ?></h1>

                        </div>
                        <div class="col-12 col-lg-8">
                            <h2><?php the_field('quilt_box_three_header', 'option'); ?></h2>
                          <div class="verdict-content">
                             <?php echo get_field('verdict_content'); ?>
                          </div>
                          <div class="button-wrap">
                            <a href="<?php echo home_url( '/notable-settlements-and-verdicts' ); ?>">VIEW ALL
                              <div class="orangeLine"></div>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                <?php endwhile; shuffle($args); ?>
                <?php endif; ?>
              <?php wp_reset_postdata(); ?>


            </div>
            <div class="col col-12 col-lg-6 column4">
              <div class="control-wrap">
                <?php the_field('fourth_quilt_box_callout', 'option'); ?>
              </div>
            </div>

          </div>

        </div>

      </div>

    </div>
    </div>
</div>
</main>

<?php get_footer(); ?>
