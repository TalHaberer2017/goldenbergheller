<?php
/* Template Name: attorneys */

get_header(); ?>
<main id="main" class="site-main index-main" role="main">
  <div class="page-wrapper">

  <div class="top-section-attorneys top-section">
    <div class="container">
      <div class="top-content">
        <div class="top-content wow fadeInDown" data-wow-delay="700">
            <?php the_field ('top_page_content'); ?>
        </div>
      </div>
    </div>
  </div>


    <div class="container">
    <div class="attorneys-list-wrapper">


      <?php
        $attorneys = array(
          'post_type' => 'attorneys',
          'post_status' => 'publish',
          'showposts' => -1
        );
        $loop = new WP_Query($attorneys);

        if ($loop->have_posts()) :?>
        <?php while($loop->have_posts()) : $loop->the_post();?>
          <div class="single-attorney wow fadeInUp" data-wow-delay="600">
            <a class="attorneyLink" href="<?php echo get_permalink(); ?>   ">
              <img class="headshot" src="<?php echo get_field('profile_image'); ?>" alt="Attorney Profile Image" />
            <div class="row">
              <div class="attorney-name">
                <h2><?php the_title(); ?></h2>
              </div>
            </div>
           </a>
          </div>

        <?php endwhile; shuffle($args); ?>
        <?php endif; ?>
      <?php wp_reset_postdata(); ?>

    </div>
    </div>

    <div class="bottom-callout-wrapper">
      <div class="container">
        <div class="row align-items-center">
          <div class="col col-12">
            <div class="attorneys-callout wow fadeInUp" data-wow-duration=".6s" data-wow-delay=".05s">

              <div class="callout-wrapper">
                <a href="<?php echo home_url( '/careers' ); ?>"><?php the_field ('attorneys_callout_text'); ?><br>
                <div class="orangeLine"></div></a>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>







</main>
<?php get_footer(); ?>
