<?php

/* Template Name: practice areas */

get_header(); ?>

<main id="main" class="site-main index-main" role="main">
<div class="page-wrapper">

  <div class="top-section">
    <div class="container">
      <div class="row text-center mx-auto">

        <div class="top-content wow fadeInDown" data-wow-duration="1s">
            <?php the_field ('top_content'); ?>
        </div>

      </div>
    </div>
  </div>

  <div class="hero-image wow fadeIn" data-wow-duration="1s" data-wow-delay=".5s">
    <img class="img-fluid mx-auto" src="<?php the_field ('hero_image'); ?>" alt="Practice Areas Goldenberg & Heller Lawyer Image">
  </div>

  <div class="practiceBack">

  </div>

  <div class="practice-box-wrapper wow slideInUp" data-wow-duration="1s" data-wow-delay=".5s">
    <div class="container p12">

        <?php if( have_rows('practice_area_box') ): ?>

      	<ul class="practice-box-grid">

      	<?php while( have_rows('practice_area_box') ): the_row();

      		// vars
      		$title = get_sub_field('practice_title');
      		$content = get_sub_field('practice_short_description');

      		?>

      		<li class="practice-box-wrap">

            <div class="practice-box">
              <h2 class="practiceTitle"><?php echo $title; ?></h2>
              <div class="practice-content">
                <?php echo $content; ?>
              </div>
              <a href="<?php the_sub_field('practice_url'); ?>">Learn More
                <div class="orangeLine"></div>
              </a>
            </div>

      		</li>

      	<?php endwhile; ?>

        <div class="circle-blue-wrapper">

          <img class="circleBlue"  src="<?php echo get_stylesheet_directory_uri(); ?>/svg/circle-blue.svg" alt="Lawyer Circle Image">


          <p class="circle-content"><?php the_field ('circle_content'); ?></p>


        </div>




      	</ul>

      <?php endif; ?>



    </div>

  </div>

  <div class="bottom-callout">
    <div class="container">
      <div class="row">
        <a class="goldberg-button mx-auto wow fadeInUp" data-wow-duration="1.5s" href="<?php echo home_url( '/contact-goldenberg-heller-antognoli/' ); ?>"><?php the_field('button_consultation', 'option'); ?></a>
      </div>
      <!-- <img src="<?php echo get_stylesheet_directory_uri(); ?>/svg/circle-blue.svg" alt=""> -->

    </div>

  </div>

</div>

</main>



<?php get_footer(); ?>
