<!doctype html>
	<html class="no-js" <?php language_attributes(); ?>>
	<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php wp_head(); ?>
	<script>

	new WOW().init();


	</script>
	</head>
	<body <?php body_class(); ?>>
	<div id="wrapper">
		<a href="#main" class="sr-only sr-only-focusable skipnav"><?php _e('Skip to main content', 'basetheme'); ?></a>
		<header id="header" class="clearfix">
			<div class="container-fluid">
				<div class="row">
					<h2 class="phoneNumber wow fadeIn" data-wow-duration="1s"><?php the_field('phone_number', 'option'); ?></h2>
				</div>
				<!-- <nav class="navbar navbar-expand-lg"> -->
					<a class="goldenbergLogo" href="<?php echo home_url( '/' ); ?>"><img class="logo wow fadeIn" data-wow-duration="1.5s" src="<?php echo get_stylesheet_directory_uri(); ?>/svg/GoldenbergLogo.svg" alt="Goldenberg Heller & Antognoli Logo"></a>
					<nav class="navbar navbar-expand-lg wow fadeInDown" data-wow-duration="1s" data-wow-delay=".2s" role="navigation">
				<!-- <div class="container-fluid"> -->
					<div class="navbar-header">
				<a class="navbar-toggler" data-toggle="collapse" data-target="#main-nav-container" aria-controls="main-nav-container" aria-expanded="false" aria-label="Toggle navigation">
					<div class="hamburger">
			    	<span></span>
						<span></span>
						<span></span>
			    </div>
				</a>
				</div>
					<?php
					wp_nav_menu(array(
						'theme_location' => 'mainnav',
						'depth' => 2,
						'container' => 'div',
						'container_class' => 'collapse navbar-collapse',
						'container_id' => 'main-nav-container',
						'menu_class' => 'nav navbar-nav',
						'li_class' => 'nav-item',
						'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
						'walker' => new WP_Bootstrap_Navwalker(),
					));
					?>
			</nav>
		<!-- </div> -->
			</div>
		</header>
