<?php get_header(); ?>

<main id="main" class="site-main index-main" role="main">
<div class="page-wrapper">
  <?php while ( have_posts() ) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <div class="hero-single-attorney">
    <!-- <img class="whiteOverlay" src="<?php echo get_stylesheet_directory_uri(); ?>/images/whiteOverlay.png" alt=""> -->
    <div class="container">
      <!-- <div class="row">
        <div class="col col-12 col-sm-8">
          <div class="attorney-top-content wow zoomIn" data-wow-delay="400">
              <?php the_content(); ?>
          </div>
        </div>
      </div> -->
    </div>
    <img class="attorneyHero wow fadeIn" data-wow-duration=".5s"  src="<?php the_field ('attorney_hero_image'); ?>"alt="Goldenberg & Heller Attorney Image">
  </div>

  <div class="attorney-information-wrapper wow fadeIn" data-wow-delay="800">
    <div class="grayback">

    </div>

    <div class="container">
      <div class="row">
        <div class="col col-12 col-md-4 columnOne">

          <div class="attorney-practice-area-box">
            <h1>Practice Areas</h1>
            <div class="areas-for-each">
              <?php

              $posts = get_field('practice_areas');

              if( $posts ): ?>
                  <ul>
                  <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                      <?php setup_postdata($post); ?>
                      <li>
                          <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                      </li>
                  <?php endforeach; ?>
                  </ul>
                  <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
              <?php endif; ?>

            </div>

            <div class="additional-fields">

              <?php if( have_rows('additional_practice_areas') ): ?>

                	<ul>

                	<?php while( have_rows('additional_practice_areas') ): the_row();

                		// vars
                		$name = get_sub_field('name');
                		$link = get_sub_field('url');

                		?>

                		<li class="slide">

                			<?php if( $link ): ?>
                				<a href="<?php echo $link; ?>">
                			<?php endif; ?>

                			 <?php echo $name; ?>

                			<?php if( $link ): ?>
                				</a>
                			<?php endif; ?>

                		</li>

                	<?php endwhile; ?>

                	</ul>

                <?php endif; ?>

            </div>

          </div>

          <div class="attorney-experience-box wow fadeIn"  data-wow-delay="700">

            <?php

        // check if the flexible content field has rows of data
        if( have_rows('qualifications') ):?>

      <?php       // loop through the rows of data
            while ( have_rows('qualifications') ) : the_row();


echo '<div class="wrap">';
                if( get_row_layout() == 'litigation_percentage' ):

          	      the_sub_field('header');
                  the_sub_field('content');

                elseif( get_row_layout() == 'education' ):

                  the_sub_field('header');
                  the_sub_field('content');

                elseif( get_row_layout() == 'bar_admissions' ):

                    the_sub_field('header');
                    the_sub_field('content');

                elseif( get_row_layout() == 'honors_and_awards' ):

                    the_sub_field('header');
                    the_sub_field('content');

                elseif( get_row_layout() == 'published_works' ):

                    the_sub_field('header');
                    the_sub_field('content');

                elseif( get_row_layout() == 'speaking_engagements' ):

                    the_sub_field('header');
                    the_sub_field('content');

                elseif( get_row_layout() == 'academic_positions' ):

                    the_sub_field('header');
                    the_sub_field('content');

                elseif( get_row_layout() == 'professional_associations_and_memberships' ):

                    the_sub_field('header');
                    the_sub_field('content');

                elseif( get_row_layout() == 'past_employment_positions' ):

                    the_sub_field('header');
                    the_sub_field('content');

                elseif( get_row_layout() == 'ancillary_businesses' ):

                    the_sub_field('header');
                    the_sub_field('content');

                elseif( get_row_layout() == 'classes_and_seminars' ):

                    the_sub_field('header');
                    the_sub_field('content');


                endif;
                echo '</div>';

         endwhile;

     endif; ?>



          </div>



        </div>

        <div class="col col-12 col-md-8 columnTwo">

          <div class="attorney-information-box">
            <div class="row">
              <div class="col col-12">
                <div class="attorney-top-content wow zoomIn" data-wow-delay="400">
                    <?php the_content(); ?>
                </div>
              </div>
              <div class="col-12 col-sm-6">
                <div class="attorney-contact">
                  <?php the_field ('attorney_contact'); ?>
                </div>
              </div>
              <div class="col-12 col-sm-6">
                <div class="vcard-wrap">
                    <a class="vcard" href="<?php the_field ('download_vcard'); ?>"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/svg/Downloadarrow.svg" alt="Goldenberg & Heller Download Image"> Download Vcard</a>
                </div>
              </div>
              </div>

          </div>

          <div class="attorney-profile wow fadeIn" data-wow-duration="1s">
            <?php the_field ('attorney_profile'); ?>
          </div>

	        <?php if (get_field("quote_box_content")) : ?>
            <div class="quote-box-wrapper wow fadeInRight" data-wow-duration=".5s">

                <?php the_field ('quote_box_content'); ?>
            </div>
        <?php endif; ?>

        </div>

      </div>

    </div>

  </div>



    <?php if( get_field('display_posts_section') ):

    $display = get_field('display_posts_section');

    ?>

      <?php if( $display ): ?>

        <div class="latest-insights-wrapper">

        <div class="latestInsights">
          <div class="container">
            <div class="row">
              <div class="col-12">

                <h2>Latest From <?php the_title(); ?> </h2>
              </div>
            </div>
            <div class="row">
              <div class="blog-list-wrapper">

            <?php

            $post_objects = get_field('attorney_posts');

            if( $post_objects ): ?>
                <div class="post-wrap">
                <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
                    <?php setup_postdata($post); ?>
                      <div class="single-post">
                        <a href="<?php echo get_permalink(); ?>">
                        <div class="image">
                          <div class="post-image-attorney">
                              <?php the_post_thumbnail(); ?>
                          </div>
                          <div class="post-meta">
                            <div class="row align-items-center">
                              <div class="col col-12">
                              <a class="title" href="<?php echo get_permalink(); ?>"><div class="post-title"><?php the_title(); ?></div>
                              </div></a>
                            </div>
                            <div class="row align-items-center row-author">
                              <div class="col col-2 col-md-3">
                                <div class="person">
                                  <?php echo get_avatar( get_the_author_meta( 'ID' ) ) ?>
                                </div>
                              </div>
                              <div class="col col-6 col-md-6">
                                <p class="authorName"> <?php the_author(); ?> </p>
                              </div>

                            </div>


                          </div>
                        </div>
                      </a>
                      </div>
                <?php endforeach; ?>
              </div>
                <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
            <?php endif;

            ?>
            </div>
          </div>


          </div>
        </div>

        <a class="goldberg-button text-center wow fadeInUp" data-wow-duration="1s" href="<?php echo home_url( '/contact-us' ); ?>">Scheduale a free consultation</a>


      </div>

			<?php endif; ?>

<?php endif; ?>






<?php if (has_post_thumbnail()) { echo maybe_ssl_url(get_the_post_thumbnail($post->ID, 'full')); } ?>
</article>

<?php
$wp_user_id = get_field('wp_user_id');
if ($wp_user_id)
{
  if (is_array($wp_user_id)) { $wp_user_id = $wp_user_id['ID']; }

  $a_query = new WP_Query(array('post_type' => 'post', 'post_status' => 'publish', 'author' => $wp_user_id, 'showposts' => 9));
  if ($a_query->have_posts()) :
  $iter = 1;
  ?>

  <?php
  endif;
  wp_reset_postdata();
}
endwhile; ?>

</div>

</main>

<?php get_footer(); ?>
