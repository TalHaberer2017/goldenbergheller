<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Boilerplate
 * @since Boilerplate 1.0
 */
?>
	<footer role="contentinfo" id="site-footer">
		<div class="container-fluid">
		<div id="footer-inner">
			<div class="row">
				<div class="col col-12 col-md-6">
					<h2>Contact Us.</h2>
					<h2 class="phoneNumber"><?php the_field('phone_number', 'option'); ?></h2>
					<p class="address"><?php the_field('address', 'option'); ?></p>
					<nav id="footer-nav-container" class="nav-container">
						<div class="row text-left">

							<div class="col col-12 col-md-6">
									<img class="whiteline" src="<?php echo get_stylesheet_directory_uri(); ?>/images/whiteline.png" alt="white line">
									<?php
									wp_nav_menu( array(
									    'menu' => 'footer-practice-areas'
									) );
									?>

							</div>

							<div class="col col-12 col-md-6">
									<img class="whiteline" src="<?php echo get_stylesheet_directory_uri(); ?>/images/whiteline.png" alt="white line">
									<?php
									wp_nav_menu( array(
											'menu' => 'footer-nav'
									) );
									?>
							</div>

						</div>

					</nav>
				</div>
				<div class="col col-12 col-md-6 homepageFooter footerOthers">

					<div class="footer-wrap">

							<img class="footerLogo" src="<?php echo get_stylesheet_directory_uri(); ?>/svg/footerLogo.svg" alt="">
							<h2>Let&#8217;s Chat.</h2>
							<p>Contact us for a consultation.</p>
							<img class="orangeLine" src="<?php echo get_stylesheet_directory_uri(); ?>/images/orangeLine.png" alt="">
							<div class="contact-button">
									<a class="button-contact" data-wow-duration="1.5s" href="<?php echo home_url( '/contact-goldenberg-heller-antognoli/' ); ?>">Contact Us</a>
							</div>
					</div>


					<!-- Homepage footer callout -->
				<div class="homepage-contact-callout">
					<a href="<?php echo home_url( '/contact-goldenberg-heller-antognoli/' ); ?>" alt="Contact Callout"><div class="blue-callout">

							<?php the_field ('homepage_footer_callout'); ?>

								<div class="orangeLine"></div>

						</div></a>
				</div>


				</div>

			</div>
			<!-- <div class="copyright">&copy; <?php echo date('Y'); ?></div> -->
		</div>
		<div id="footer-bottom" class="footer-bottom">
			<a class="goldenbergLogo" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/svg/Goldenberg_Logo.svg" alt="Goldenberg Heller & Antognoli Logo"></a>
			<div class="copyright text-center">
					<?php the_field ('copyright_text', 'options'); ?>
			</div>
			<div class="social-icons-row">
				<ul class="mx-auto d-block">
						<li><a href="<?php the_field('facebook_link', 'option'); ?>" target="_blank"><img class="social-logo facebookLogo" src="<?php echo get_stylesheet_directory_uri(); ?>/svg/facebook-logo.svg"  alt="facebook logo"></a></li>
						<li><a href="<?php the_field('linkedin_link', 'option'); ?>" target="_blank"><img class="social-logo linkedinLogo" src="<?php echo get_stylesheet_directory_uri(); ?>/svg/linkedin-logo.svg" alt="linkedIn logo"></a></li>
						<li><a href="<?php the_field('twitter_link', 'option'); ?>" target="_blank"><img class="social-logo twitterLogo" src="<?php echo get_stylesheet_directory_uri(); ?>/svg/twitter-logo.svg" alt="twitter logo"></a></li>
				</ul>
			</div>

		</div>
	</div>
	</footer>
<?php wp_footer(); ?>
</div>
</body>
</html>
