<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php wp_head(); ?>
<script>

          // Google Analytics

          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){

          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),

          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)

          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');



          ga('create', 'UA-15293811-1', 'auto');

          ga('send', 'pageview');

</script>
<script>
			new WOW().init();
</script>
</head>
<body <?php body_class(); ?>>
<div id="wrapper">
	<a href="#main" class="sr-only sr-only-focusable skipnav"><?php _e('Skip to main content', 'basetheme'); ?></a>
	<header id="header" class="clearfix">
		<h2 class="phoneNumber wow fadeIn" data-wow-duration="1s"><?php the_field('phone_number', 'option'); ?></h2>
		<div class="container-fluid">
			<nav id="mainNav" class="navbar navbar-expand-lg">
				<a class="goldenbergLogo" href="<?php echo home_url( '/' ); ?>"><img class="logo wow fadeIn" data-wow-duration="1.5s" src="<?php echo get_stylesheet_directory_uri(); ?>/svg/Goldenberg_Logo.svg" alt="Goldenberg Heller & Antognoli Logo"></a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav-container" aria-controls="main-nav-container" aria-expanded="false" aria-label="Toggle navigation">
			    <div class="hamburger">
			    	<span></span>
						<span></span>
						<span></span>
			    </div>
			  </button>
				<div class="collapse navbar-collapse" id="main-nav-container">

					<ul class="navbar-nav main-nav wow fadeIn" data-wow-duration="1.2s">
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="<?php echo home_url( '/' ); ?>" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								About Us
							</a>
							<div class="dropdown-menu drop-one" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="<?php echo home_url( '/about-goldenberg-heller-antognoli' ); ?>">About GHA</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="<?php echo home_url( '/referral-partners' ); ?>">Referral Partners</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="<?php echo home_url( '/careers' ); ?>">Careers</a>
								<div class="dropdown-divider"></div>
							</div>
						</li>
					 <!-- <li class="nav-item active">
						 <a class="nav-link" href="<?php echo home_url( '/about-us' ); ?>">About Us <span class="sr-only">(current)</span></a>
					 </li> -->
					 <li class="nav-item">
						 <a class="nav-link" href="<?php echo home_url( '/our-attorneys' ); ?>">Our Attorneys</a>
					 </li>
					 <li class="nav-item dropdown">
						 <a class="nav-link dropdown-toggle noHover" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							 Practice Areas
						 </a>
						 <div class="dropdown-menu drop-two" aria-labelledby="navbarDropdown1">
							 <a class="dropdown-item" href="<?php echo home_url( '/practice-areas' ); ?>">Practice Areas Overview</a>
							 <div class="dropdown-divider"></div>
							 <a class="dropdown-item" href="<?php echo home_url( '/practice-areas/Mesothelioma' ); ?>">Mesothelioma</a>
							 <div class="dropdown-divider"></div>
							 <a class="dropdown-item" href="<?php echo home_url( '/practice-areas/personal-injury' ); ?>">Personal Injury</a>
							 <div class="dropdown-divider"></div>
							 <a class="dropdown-item" href="<?php echo home_url( '/business-and-commercial-law' ); ?>">Business & Commercial Law</a>
							 <div class="dropdown-divider"></div>
							 <a class="dropdown-item" href="<?php echo home_url( '/practice-areas/real-estate' ); ?>">Real Estate</a>
							 <div class="dropdown-divider"></div>
							 <a class="dropdown-item" href="<?php echo home_url( '/practice-areas/class-action-lawsuits/' ); ?>">Class Actions</a>
							 <div class="dropdown-divider"></div>
							 <a class="dropdown-item" href="<?php echo home_url( '/practice-areas/trusts-and-estate-planning' ); ?>">Trusts and Estate Planning</a>
							 <div class="dropdown-divider"></div>
								  <a class="dropdown-item" href="<?php echo home_url( '/practice-areas/intellectual-property' ); ?>">Intellectual Property</a>
							</div>
					 </li>

					 <li class="nav-item">
						 <a class="nav-link" href="<?php echo home_url( '/news-and-insights' ); ?>">News & Insights</a>
					 </li>
					 <li class="nav-item">
						<a class="nav-link" href="<?php echo home_url( '/contact-goldenberg-heller-antognoli/' ); ?>">Contact Us&nbsp;&nbsp; <img class="image-line" src="<?php echo get_stylesheet_directory_uri(); ?>/images/grayLine.png" alt=""></a>
					</li>
					<form class="formSearch" role="search" class="form-inline my-2 my-lg-0 search-form">
						 <input class="form-control mr-sm-2" type="search" aria-label="Search">
						 <button class="btn my-2 my-sm-0 search-button" type="submit"><img src="<?php echo get_stylesheet_directory_uri(); ?>/svg/Magnifying_glass.svg" alt="search icon"></button>
					 </form>

				 </ul>

					<!-- <?php
					/*
						May need to remove the BootstrapNavMenuWalker and make menus a different way.
						This entire header/nav setup is from Bootstrap 3, haven't tested with Bootstrap 4 yet
					*/
					$args = array(
						'theme_location' => 'mainnav',
						'depth'		 => 0,
						'container'	 => false,
						'menu_class'	 => 'navbar-nav mr-auto',
						'menu_id' => 'main-nav',
						'walker'	 => new BootstrapNavMenuWalker()
					);
					wp_nav_menu($args);
				?> -->
				</div>
			</nav>
		</div>
	</header>
